import pytest

from interledger.adapter.state_manager import LocalILStateManager
from interledger.transfer import TransferStatus, Transfer, DecentralizedTransferStatus, DecentralizedTransfer


@pytest.mark.asyncio
class TestLocalStateManager:
    def test_manager_initialization(self):
        local_mng = LocalILStateManager()

        assert type(local_mng.local_transfers) == dict
        assert not local_mng.local_transfers

        assert type(local_mng.transfers_ready) == dict
        assert len(local_mng.transfers_ready) == 0

        assert type(local_mng.transfers_responded) == dict
        assert len(local_mng.transfers_responded) == 0

    async def test_manager_create_entry(self):
        local_mng = LocalILStateManager()
        t1, t2 = DecentralizedTransfer(), DecentralizedTransfer()
        t1.payload = {"nonce": "1001",
                      "data": "0xabc123"}

        res1 = await local_mng.create_entry("101", t1)

        assert res1 == True
        assert local_mng.local_transfers['101']
        t_DSM = local_mng.local_transfers['101']

        assert type(t_DSM) == DecentralizedTransfer
        assert t_DSM.status == DecentralizedTransferStatus.Initialized # DecentralizedTransferStatus.Initialized

        res2 = await local_mng.create_entry("101", t2)
        assert res2 == False

    async def test_manager_signal_send_accept(self):
        local_mng = LocalILStateManager()
        local_mng.local_transfers['101'] = DecentralizedTransfer(DecentralizedTransferStatus.InitializedEndorsed)

        t_DSM = local_mng.local_transfers['101']
        assert type(t_DSM) == DecentralizedTransfer
        assert t_DSM.status == DecentralizedTransferStatus.InitializedEndorsed

        await local_mng.signal_send_acceptance('101')

        assert t_DSM.status == DecentralizedTransferStatus.Sent
        assert t_DSM.send_accepted == True

    async def test_manager_signal_process_accept(self):
        local_mng = LocalILStateManager()
        local_mng.local_transfers['101'] = t_DSM = DecentralizedTransfer()

        t_DSM.status = DecentralizedTransferStatus.AcceptedEndorsed

        await local_mng.signal_confirm_acceptance('101')

        assert t_DSM.status == DecentralizedTransferStatus.Confirming
        assert t_DSM.confirm_accepted == True

    async def test_manager_update_entry(self):
        local_mng = LocalILStateManager()
        local_mng.local_transfers['101'] = t_DSM = \
            DecentralizedTransfer(DecentralizedTransferStatus.Sent)

        t_DSM.payload = {"nonce": "1001",
                         "data": "0xabc123"}

        t_next = DecentralizedTransfer( \
            DecentralizedTransferStatus.Accepted)
        t_next.result = {"commit_status": True,
                         "commit_tx_hash": "0xcommit_tx_hash"}

        await local_mng.update_entry('101',
                                     t_next.status,
                                     t_next)

        assert t_DSM.status == DecentralizedTransferStatus.Accepted

