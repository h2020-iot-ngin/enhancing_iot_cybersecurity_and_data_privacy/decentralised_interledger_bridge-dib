import pytest
import asyncio
from copy import deepcopy
from uuid import uuid4

from interledger.transfer import Transfer, TransferStatus, DecentralizedTransfer, DecentralizedTransferStatus
from interledger.adapter.state_manager import LocalILStateManager

from .utils import MockInitiator, MockResponder, MockResponderAbort
from interledger.dil import DecentralizedInterledger


# Overview
# MockInitiator <- Interledeger -> MockResponder
#               (Local state manager)

# test DIL receive transfer
@pytest.mark.asyncio
async def test_dil_receive_transfer():
    # initialize
    t = Transfer()
    t.payload = {'id': '001', 'data': '0xdummy_data'}

    init = MockInitiator([t])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)

    assert not dil.state_manager.local_transfers
    assert not dil.state_manager.transfers_ready
    assert not dil.state_manager.transfers_responded

    # start
    task = asyncio.ensure_future(dil.receive_transfer())
    assert task.done() == False
    res = await task

    assert init.events == []
    assert len(dil.transfers) == 1
    assert res == 1
    assert all(t.status == TransferStatus.INIT for t in dil.transfers.values())

# test DIL create entry
@pytest.mark.asyncio
async def test_dil_create_entry():
    # initialize
    t = Transfer()
    t.payload = {'id': '001', 'data': '0xdummy_data'}
    t.payload['nonce'] = nonce = str(uuid4().int)
    t.metadata = {}

    init = MockInitiator([])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)
    dil.transfers = {nonce: t}

    # check local transfers not populated
    assert len(dil.state_manager.local_transfers) == 0

    # start
    task = asyncio.ensure_future(dil.create_entry())
    assert task.done() == False
    res = await task

    assert res == 1
    assert dil.state_manager.local_transfers
    assert len(dil.state_manager.local_transfers) == 1

    local_transfer = dil.state_manager.local_transfers['001']
    assert isinstance(local_transfer, DecentralizedTransfer)

    assert local_transfer.payload['id']
    assert local_transfer.payload['nonce']
    assert local_transfer.payload['data'] == '0xdummy_data'

    assert local_transfer.status == DecentralizedTransferStatus.Initialized


# test DIL validate entry for transfers created
@pytest.mark.asyncio
async def test_dil_validate_entry_for_transfers_created():
    # initialize
    t = Transfer()
    t.payload = {'id': '001', 'data': '0xdummy_data'}
    t.payload['nonce'] = nonce = str(uuid4().int)
    t.metadata = {}

    dt = DecentralizedTransfer(
        DecentralizedTransferStatus.Initialized, t)

    init = MockInitiator([])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)

    dil.transfers = {nonce: t}
    dil.state_manager.local_transfers = {'001': deepcopy(dt)}
    
    local_transfer = dil.state_manager.local_transfers['001']
    dil.state_manager.transfers_created['001'] = local_transfer

    # start
    task = asyncio.ensure_future(dil.validate_entry())
    assert task.done() == False
    res = await task

    # check transfer endorsed correctly
    assert local_transfer.endorsement == 1
    assert local_transfer.is_endorsed == True
    assert local_transfer.is_declined == False

    # check transferss created cleaned up
    assert not dil.state_manager.transfers_created



# test DIL send transfer
@pytest.mark.asyncio
async def test_dil_send_transfer():
    # initialize
    t = Transfer()
    t.payload = {'id': '001', 'data': '0xdummy_data'}
    t.payload['nonce'] = nonce = str(uuid4().int)
    t.metadata = {}

    dt = DecentralizedTransfer(
        DecentralizedTransferStatus.InitializedEndorsed, t)

    init = MockInitiator([])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)

    dil.transfers = {nonce: t}
    dil.state_manager.local_transfers = {'001': deepcopy(dt)}
    dil.state_manager.transfers_ready['001'] = dt

    assert len(dil.transfers_sent) == 0

    # start
    task = asyncio.ensure_future(dil.send_transfer())
    assert task.done() == False
    await task

    # check cache updated
    assert dil.transfers_sent
    assert len(dil.transfers_sent) == 1

    transfer_sent = dil.transfers_sent[0]

    assert isinstance(transfer_sent, Transfer)
    assert transfer_sent.status == TransferStatus.SENT
    assert transfer_sent.payload == t.payload
    assert asyncio.isfuture(transfer_sent.send_task)

    # check DSM layer updated
    assert dil.state_manager.local_transfers
    assert len(dil.state_manager.local_transfers) == 1
    
    local_transfer = dil.state_manager.local_transfers['001']

    assert local_transfer.payload['id']
    assert local_transfer.payload['nonce']
    assert local_transfer.payload['data'] == '0xdummy_data'

    assert local_transfer.status == DecentralizedTransferStatus.Sent

    await transfer_sent.send_task
    assert transfer_sent.send_task.done() == True

    # check transfers ready cleaned up
    assert not dil.state_manager.transfers_ready

# Test DIL transfer result
@pytest.mark.asyncio
async def test_dil_transfer_result():
    # initialize
    t = Transfer()
    t.status = TransferStatus.SENT
    t.payload = {'id': '001', 'data': '0xdummy_data'}
    t.payload['nonce'] = str(uuid4().int)
    t.metadata = {}

    dt = DecentralizedTransfer(
        DecentralizedTransferStatus.Sent, t)

    async def foo():
        return {"status": True}

    t.send_task = asyncio.ensure_future(foo()) # send task not stored on DSM layer

    init = MockInitiator([])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)

    dil.transfers = [t]
    dil.transfers_sent = [t]
    
    dil.state_manager.local_transfers = {'001': deepcopy(dt)}

    assert len(dil.transfers_sent) == 1
    assert not t.result

    # start
    task = asyncio.ensure_future(dil.transfer_result())
    assert task.done() == False
    await task

    # check local transfers updated
    assert t.send_task.done() == True

    assert dil.state_manager.local_transfers
    assert len(dil.state_manager.local_transfers) == 1

    local_transfer = dil.state_manager.local_transfers['001']

    assert local_transfer.status == DecentralizedTransferStatus.Accepted

    # check transfers sent cleaned up
    assert not dil.transfers_sent

    # TODO: to be moved to test of other method
    # # check for transfer responded
    # assert len(dil.state_manager.transfers_responded) == 1
    # transfer_responded = dil.transfers[0]

    # assert transfer_responded.status == TransferStatus.RESPONDED
    # assert transfer_responded.result == 42

    # assert not dil.transfers_sent


# test DIL validate entry for transfers responded
@pytest.mark.asyncio
async def test_dil_validate_entry_for_transfers_responded():
    # initialize
    t = Transfer()
    t.staus = TransferStatus.SENT
    t.payload = {'id': '001', 'data': '0xdummy_data'}
    t.payload['nonce'] = nonce = str(uuid4().int)
    t.metadata = {}

    dt = DecentralizedTransfer(
        DecentralizedTransferStatus.Accepted, t)

    init = MockInitiator([])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)

    dil.transfers = {nonce: t}
    dil.state_manager.local_transfers = {'001': deepcopy(dt)}
    
    local_transfer = dil.state_manager.local_transfers['001']
    dil.state_manager.transfers_responded['001'] = local_transfer

    # start
    task = asyncio.ensure_future(dil.validate_entry())
    assert task.done() == False
    res = await task

    # check transfer endorsed correctly
    assert local_transfer.endorsement == 1
    assert local_transfer.is_endorsed == True
    assert local_transfer.is_declined == False

    # check transferss created cleaned up
    assert not dil.state_manager.transfers_responded



# Test DIL process result for commit
@pytest.mark.asyncio
async def test_dil_process_result_commit():
    # initialize
    t = Transfer()
    t.status = TransferStatus.RESPONDED
    t.payload = {'id': '001', 'data': '0xdummy_data'}
    t.payload['nonce'] = nonce = str(uuid4().int)
    t.result = {'status': True}
    t.metadata = {}

    dt = DecentralizedTransfer(DecentralizedTransferStatus.AcceptedEndorsed) # could be RejectedEndorsed
    dt.payload = {'id': '001', 'data': '0xdummy_data'}
    dt.payload['nonce'] = str(uuid4().int)

    init = MockInitiator([])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)
    dil.transfers = [t]
    dil.transfers_responded = [t]
    dil.state_manager.local_transfers = {'001': deepcopy(dt)}
    
    async def foo():
        return 42

    t.confirm_task = asyncio.ensure_future(foo()) # confirm task not stored on DSM layer

    assert not dil.results_committing

    # start
    task = asyncio.ensure_future(dil.process_result())
    assert task.done() == False
    await task

    # check for transfer confirming
    assert len(dil.results_committing) == 1
    result_confirming = dil.results_committing[0]

    assert result_confirming.status == TransferStatus.CONFIRMING
    assert result_confirming.result['status'] == True
    assert asyncio.isfuture(result_confirming.confirm_task)

    assert len(dil.results_commit) == 0
    assert len(dil.results_abort) == 0

    # check entry at DSM updated as well
    local_transfer = dil.state_manager.local_transfers['001']
    assert local_transfer.status == DecentralizedTransferStatus.Confirming

    await result_confirming.confirm_task
    assert result_confirming.confirm_task.done() == True

    # check transfers responded cleaned up
    assert not dil.state_manager.transfers_responded


# # Test DIL process result for abort
# @pytest.mark.asyncio
# async def test_dil_process_result_abort():
#     # initialize
#     t = Transfer()
#     t.status = TransferStatus.RESPONDED
#     t.payload = {'id': '001', 'data': '0xdummy_data'}
#     t.payload['nonce'] = str(uuid4().int)
#     t.result = {'status': False}

#     init = MockInitiator([])
#     resp = MockResponder()
#     state_manager = LocalILStateManger()

#     dil = DecentralizedInterledger(init, resp, state_manager)
#     dil.state_manager.local_transfers = {'001': deepcopy(t)}
#     dil.state_manager.transfers_responded = [t]

#     async def foo():
#         return 42

#     t.confirm_task = asyncio.ensure_future(foo())
#     dil.transfers = [t]

#     # start
#     task = asyncio.ensure_future(dil.process_result())
#     assert task.done() == False
#     await task

#     # check local transfers updated
#     assert dil.state_manager.local_transfers
#     assert len(dil.state_manager.local_transfers) == 1

#     local_transfer = dil.state_manager.local_transfers['001']

#     assert local_transfer.status == TransferStatus.CONFIRMING
#     assert local_transfer.result['status'] == False

#     # check for transfer confirming
#     assert len(dil.results_aborting) == 1
#     result_confirming = dil.results_aborting[0]

#     assert result_confirming.status == TransferStatus.CONFIRMING
#     assert result_confirming.result['status'] == False
#     assert asyncio.isfuture(result_confirming.confirm_task)

#     assert len(dil.results_commit) == 0
#     assert len(dil.results_abort) == 0

#     await result_confirming.confirm_task
#     assert result_confirming.confirm_task.done() == True

#     assert len(dil.state_manager.transfers_responded) == 0


# Test DIL confirm_transfer with a commit
@pytest.mark.asyncio
async def test_dil_confirm_transfer_commit():
    # initialize
    t = Transfer()
    t.status = TransferStatus.CONFIRMING
    t.payload = {'id': '001', 'data': '0xdummy_data'}
    t.payload['nonce'] = str(uuid4().int)
    t.result = {'status': True, 'blockNumber': "123", 'commit_tx_hash': "0x123"}
    t.metadata = {}

    dt = DecentralizedTransfer(DecentralizedTransferStatus.Confirming) # could be RejectedEndorsed
    dt.payload = {'id': '001', 'data': '0xdummy_data'}
    dt.payload['nonce'] = str(uuid4().int)

    init = MockInitiator([])
    resp = MockResponder()
    state_manager = LocalILStateManager()

    dil = DecentralizedInterledger(init, resp, state_manager)

    async def foo():
        return {'commit_status': True,
                'commit_tx_hash': '0x333'}

    t.confirm_task = asyncio.ensure_future(foo())
    dil.transfers = [t]
    dil.results_committing = [t]
    dil.state_manager.local_transfers = {'001': deepcopy(dt)}

    # start
    task = asyncio.ensure_future(dil.confirm_transfer())
    assert task.done() == False
    print("t:", t.result)
    await task

    # check local transfers updated
    assert dil.state_manager.local_transfers
    assert len(dil.state_manager.local_transfers) == 1

    local_transfer = dil.state_manager.local_transfers['001']

    assert local_transfer.status == DecentralizedTransferStatus.CommittedEndorsed

    # check for transfer confirming
    assert len(dil.results_commit) == 1
    result_commit = dil.results_commit[0]

    assert result_commit.status == TransferStatus.FINALIZED
    assert result_commit.result['commit_status'] == True
    assert result_commit.result['commit_tx_hash'] == '0x333'
    assert asyncio.isfuture(result_commit.confirm_task)

    assert len(dil.results_committing) == 0
    assert len(dil.results_abort) == 0


# # Test DIL confirm_transfer with a abort
# @pytest.mark.asyncio
# async def test_dil_confirm_transfer_abort():
#     # initialize
#     t = Transfer()
#     t.status = TransferStatus.CONFIRMING
#     t.payload = {'id': '001', 'data': '0xdummy_data'}
#     t.payload['nonce'] = str(uuid4().int)
#     t.result = {}

#     init = MockInitiator([])
#     resp = MockResponder()
#     state_manager = LocalILStateManager()

#     dil = DecentralizedInterledger(init, resp, state_manager)
#     dil.state_manager.local_transfers = {'001': deepcopy(t)}

#     async def foo():
#         return {'abort_status': True,
#                 'abort_tx_hash': '0x444'}

#     t.confirm_task = asyncio.ensure_future(foo())
#     dil.results_aborting = [t]

#     # start
#     task = asyncio.ensure_future(dil.confirm_transfer())
#     assert task.done() == False
#     await task

#     # check local transfers updated
#     assert dil.state_manager.local_transfers
#     assert len(dil.state_manager.local_transfers) == 1

#     local_transfer = dil.state_manager.local_transfers['001']

#     assert local_transfer.status == TransferStatus.FINALIZED
#     assert local_transfer.result['abort_status'] == True

#     # check for transfer confirming
#     assert len(dil.results_abort) == 1
#     result_confirming = dil.results_abort[0]

#     assert result_confirming.status == TransferStatus.FINALIZED
#     assert result_confirming.result['abort_status'] == True
#     assert result_confirming.result['abort_tx_hash'] == '0x444'
#     assert asyncio.isfuture(result_confirming.confirm_task)

#     assert len(dil.results_aborting) == 0
#     assert len(dil.results_commit) == 0
