import pytest
import asyncio
from copy import deepcopy
from uuid import uuid4

from interledger.transfer import Transfer, TransferStatus, DecentralizedTransfer, DecentralizedTransferStatus
from interledger.adapter.state_manager import LocalILStateManager

from .utils import MockInitiator, MockResponder, MockResponderAbort
from interledger.dil import DecentralizedInterledger


@pytest.mark.asyncio
async def test_dil_run():
    print("\n=== Integration test complete run ===")
    t = Transfer()
    t.payload = {
        "nonce": str(uuid4().int), \
        "id": '1', \
        "data": b"dummy"
    }

    init = MockInitiator([t])
    resp = MockResponder()
    state_manager = LocalILStateManager()
    dil = DecentralizedInterledger(init, resp, state_manager)

    task = asyncio.ensure_future(dil.run())

    await asyncio.sleep(1)   # Simulate interledger running

    assert len(dil.transfers) == 0
    assert len(dil.transfers_sent) == 0
    assert len(dil.transfers_responded) == 0
    assert len(dil.results_committing) == 0
    assert len(dil.results_aborting) == 0
    # below should not equal to 4, since transfer with repeated id will be rejected
    assert len(dil.results_commit) == 1

    print("$$$$$$$$$$$$$$$$$$$")
    t_res = dil.results_commit[0]
    print(t_res)
    print(t_res.payload)
    print(t_res.status)
    print("$$$$$$$$$$$$$$$$$$$")

    # below should not equal to 2, since transfer with repeated id will be rejected
    assert len(dil.results_abort) == 0

    dil.stop()
    await task
