import pytest
import asyncio
from unittest.mock import patch
from web3 import Web3
from uuid import uuid4

from .test_setup import setUp, setup_provider, create_token, accept_token, transfer_token
from interledger.dil import DecentralizedInterledger
from interledger.adapter.ethereum import EthereumInitiator, EthereumResponder
from interledger.adapter.state_manager import LocalILStateManager

"""Overview
Source endpoint (A) <- Initiator <- DIL -> Responder -> Destination endpoint (B)
                                 (DSM layer)
"""

@pytest.mark.asyncio
async def test_dil_with_two_ethereum(config_file):
    # set up game token contracts on 
    tokenId = uuid4().int
    
    cfg_A = setUp(config_file, 'left')
    cfg_B = setUp(config_file, 'right')

    w3_A = setup_provider(cfg_A.url, cfg_A.port, cfg_A.poa, cfg_A.ipc_path)
    w3_B = setup_provider(cfg_B.url, cfg_B.port, cfg_B.poa, cfg_B.ipc_path)
    
    token_instance_A = w3_A.eth.contract(abi=cfg_A.contract_abi, address=cfg_A.contract_address)
    token_instance_B = w3_B.eth.contract(abi=cfg_B.contract_abi, address=cfg_B.contract_address)
    
    await create_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
    await create_token(cfg_B.minter, token_instance_B, w3_B, tokenId)
    assert token_instance_B.functions.getStateOfToken(tokenId).call() == 0
    assert token_instance_A.functions.getStateOfToken(tokenId).call() == 0
    print("Test setup ready")

    with patch("interledger.interledger.Interledger.cleanup") as mock_cleanup:
        # mock cleanup to check the transfer reaches the end

        # Create interledger with a bridge in direction from A to B
        print("Building Interledger bridge A -> B")
        initiator = EthereumInitiator(cfg_A)
        responder = EthereumResponder(cfg_B)
        state_manager = LocalILStateManager()
        dil = DecentralizedInterledger(initiator, responder, state_manager)

        print("Creating Intelredger run coroutine")
        interledger_task = asyncio.ensure_future(dil.run())

        # Activate token in ledgerA
        print("A smart contract call in ledger A marks the token in 'TransferOut'")
        await accept_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 2 # Here
        #print("A: Here")

        (data, blockNumber, gas_used) = await transfer_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 1 # TransferOut
        #print("A: TransferOut")
        
        await asyncio.sleep(5) # Simulate Interledger running, should be longer than block time

        assert token_instance_B.functions.getStateOfToken(tokenId).call() == 2 # Here
        #print("B: Here")
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 0 # Not Here
        #print("A: Not Here")

        print("Stopping Interledger run coroutine")
        dil.stop()
        await interledger_task

        print("*----------*")


        print("Building Interledger bridge B -> A")
        initiator = EthereumInitiator(cfg_B)
        responder = EthereumResponder(cfg_A)
        state_manager = LocalILStateManager()
        dil = DecentralizedInterledger(initiator, responder, state_manager)

        print("Creating Interledger run coroutine")
        interledger_task = asyncio.ensure_future(dil.run())

        print("A smart contract call in ledger B marks the token in 'TransferOut'")
        (data, blockNumber, gas_used) = await transfer_token(cfg_B.minter, token_instance_B, w3_B, tokenId)
        assert token_instance_B.functions.getStateOfToken(tokenId).call() == 1

        await asyncio.sleep(5) # Simulate Interledger running, should be longer than block time

        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 2
        assert token_instance_B.functions.getStateOfToken(tokenId).call() == 0

        print("Stopping Interledger run coroutine")
        dil.stop()
        await interledger_task
