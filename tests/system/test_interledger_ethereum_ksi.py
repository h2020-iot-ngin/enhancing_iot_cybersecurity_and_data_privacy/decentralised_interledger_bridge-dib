from interledger.interledger import Interledger
from interledger.transfer import Transfer
from interledger.adapter.ethereum import EthereumInitiator
from .test_setup import setUp, setup_provider, setUp_ksi, create_token, accept_token, transfer_token
from interledger.adapter.ksi import KSIResponder
from web3 import Web3
from unittest.mock import patch
import pytest
import asyncio
import requests
from uuid import uuid4

# # # # Global view
# # #
# # #  LedgerA <- Initiator <- Interledeger -> Responder -> LedgerB
# #


@pytest.mark.asyncio
async def test_interledger_with_two_ethereum(config_file):
    # set up ledgerA and ledgerB
    tokenId = uuid4().int
    cfg = setUp(config_file, 'left')
    w3 = setup_provider(cfg.url, cfg.port, cfg.poa, cfg.ipc_path)
    
    token_instance = w3.eth.contract(abi=cfg.contract_abi, address=cfg.contract_address)
    (tokenId,cost) = await create_token(cfg.minter, token_instance, w3, tokenId)

    (url_ksi, hash_algorithm, username, password) = setUp_ksi(config_file)

    print("Test setup ready")

    with patch("interledger.interledger.Interledger.cleanup") as mock_cleanup:
        # mock cleanup to check the transfer reaches the end

        # Create interledger with a bridge in direction from A to B
        print("Building Interledger bridge A -> B")
        init = EthereumInitiator(cfg)
        responder = KSIResponder(url_ksi, hash_algorithm, username, password)
        interledger = Interledger(initiator, responder)

        print("Creating Intelredger run coroutine")
        

        interledger_task = asyncio.ensure_future(interledger.run())

        print("A smart contract call in ledger A marks the token in 'TransferOut'")

        # Activate token in ledgerA

        await accept_token(contract_minter, token_instance, w3, tokenId)
        assert token_instance.functions.getStateOfToken(tokenId).call() == 2
        await transfer_token(contract_minter, token_instance, w3, tokenId)
        assert token_instance.functions.getStateOfToken(tokenId).call() == 1
       
        await asyncio.sleep(2) # Simulate Interledger running

        assert token_instance.functions.getStateOfToken(tokenId).call() == 0
        assert len(interledger.transfers) == 1
        assert len(interledger.transfers_sent) == 1
        
        url = responder.url + "/" + interledger.transfers_sent[0].result["tx_hash"]
        # send it as a GET request and check for the result
        ksi_request = requests.get(url, auth=(responder.username, responder.password))
        assert ksi_request.status_code == 200
        assert ksi_request.json()['verificationResult']['status'] == "OK"

        assert len(interledger.results_commit) == 1


        print("Stopping Interledger run coroutine")
        interledger.stop()
        await interledger_task


        print("*----------*")

