from uuid import uuid4
from web3 import Web3
import pytest, asyncio
from unittest.mock import patch

from src.interledger.dil import DecentralizedInterledger
from src.interledger.adapter.state_manager import EthereumILStateManager, LocalILStateManager
from src.interledger.adapter.ethereum import EthereumInitiator, EthereumResponder
from .test_setup import setUp, setup_provider, create_token, accept_token, transfer_token


# # # # Global view
# # #  LedgerA <- Initiator <- Interledeger -> Responder -> LedgerB
@pytest.mark.asyncio
async def test_interledger_with_two_ethereum(config_file, config_file_2):
    # set up ledgerA and ledgerB
    tokenId = uuid4().int
    
    cfg_A = setUp(config_file, 'left')
    cfg_B = setUp(config_file, 'right')
    cfg_DSM_1 = setUp(config_file, 'dsm1')
    cfg_DSM_2 = setUp(config_file_2, 'dsm1')

    w3_A = setup_provider(cfg_A.url, cfg_A.port, cfg_A.poa, cfg_A.ipc_path)
    w3_B = setup_provider(cfg_B.url, cfg_B.port, cfg_B.poa, cfg_B.ipc_path)
    
    token_instance_A = w3_A.eth.contract(abi=cfg_A.contract_abi, address=cfg_A.contract_address)
    token_instance_B = w3_B.eth.contract(abi=cfg_B.contract_abi, address=cfg_B.contract_address)
    
    await create_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
    await create_token(cfg_B.minter, token_instance_B, w3_B, tokenId)
    assert token_instance_B.functions.getStateOfToken(tokenId).call() == 0
    assert token_instance_A.functions.getStateOfToken(tokenId).call() == 0

    print("Test setup ready")

    with patch("interledger.interledger.Interledger.cleanup") as mock_cleanup:
        # mock cleanup to check the transfer reaches the end

        # Create interledger with a bridge in direction from A to B
        print("Building Interledger bridge A -> B")
        initiator_1 = EthereumInitiator(cfg_A)
        initiator_2 = EthereumInitiator(cfg_A)

        responder_1 = EthereumResponder(cfg_B)
        responder_2 = EthereumResponder(cfg_B)

        if cfg_DSM_1.minter == "LOCAL": # local DSM manager
            state_manager_1 = LocalILStateManager()
        else:
            state_manager_1 = EthereumILStateManager(cfg_DSM_1)

        if cfg_DSM_2.minter == "LOCAL": # local DSM manager
            state_manager_2 = LocalILStateManager()
        else:
            state_manager_2 = EthereumILStateManager(cfg_DSM_2)

        dib_1 = DecentralizedInterledger(initiator_1, responder_1, state_manager_1)
        dib_2 = DecentralizedInterledger(initiator_2, responder_2, state_manager_2)

        print("Creating Intelredger run coroutine")

        future_dib_1 = asyncio.ensure_future(dib_1.run())
        future_dib_2 = asyncio.ensure_future(dib_2.run())
        dib_task = asyncio.gather(future_dib_1, future_dib_2)

        print("A smart contract call in ledger A marks the token in 'TransferOut'")

        # Activate token in ledger A
        await accept_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 2

        # Transfer active state from ledger A to B
        (data, blockNumber, gas_used) = await transfer_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 1
       
        await asyncio.sleep(20) # Simulate Interledger running, should be longer than block time

        assert token_instance_B.functions.getStateOfToken(tokenId).call() == 2 # Here
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 0 # Not Here

        print("Stopping Interledger run coroutine")
        dib_1.stop()
        dib_2.stop()
        await dib_task
