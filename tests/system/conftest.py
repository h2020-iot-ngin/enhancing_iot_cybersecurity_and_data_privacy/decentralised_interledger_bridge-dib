import pytest

# def pytest_addoption(parser):
#     parser.addoption(
#         "--config", action="store", help="configuration file path"
#     )


# @pytest.fixture
# def config(request):
#     return request.config.getoption("--config")

@pytest.fixture
def config():
    return "local-config.cfg"

@pytest.fixture
def dib_config():
    return "configs/dib-node-1.cfg"
    #return "local-dib-config.cfg"

def pytest_addoption(parser):
    parser.addoption("--config_file", action="store", default="configs/local-config.cfg")
    parser.addoption("--config_file_2", action="store", default="configs/dib-node-2.cfg")

def pytest_generate_tests(metafunc):
    option_value = metafunc.config.option.config_file
    if 'config_file' in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("config_file", [option_value])

    option_value = metafunc.config.option.config_file_2
    if 'config_file_2' in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("config_file_2", [option_value])
