import pytest, asyncio
from unittest.mock import patch
from web3 import Web3
from uuid import uuid4
from eth_abi import encode_abi

from interledger.adapter.interfaces import ErrorCode
from interledger.interledger import Interledger
from interledger.adapter.ethereum import EthereumInitiator, EthereumResponder
from .test_setup import setUp, setup_provider, create_token, accept_token, transfer_token


# Set this variable to True to execute these tests
# Remember to have an ethereum instance with mining time *NOT* automatic (like in ganache)
ENABLE_THIS_TESTS = True

@pytest.mark.asyncio
async def test_responder_receive_timeout(config_file):

    if not ENABLE_THIS_TESTS:
        return 

    cfg = setUp(config_file, 'left')
    w3 = setup_provider(cfg.url, cfg.port, cfg.poa, cfg.ipc_path)
    
    token_instance = w3.eth.contract(abi=cfg.contract_abi, address=cfg.contract_address)
    (tokenId,cost) = await create_token(cfg.minter, token_instance, w3)
    assert token_instance.functions.getStateOfToken(tokenId).call() == 0
    ### Test Ethereum Responder ###
    
    resp = EthereumResponder(cfg)
    
    data = encode_abi(['uint256'], [tokenId])
    nonce = "42"
    resp.timeout = 0
    result = await resp.send_data(nonce, data)
    assert result["status"] == False
    assert result["error_code"] == ErrorCode.TIMEOUT

@pytest.mark.asyncio
async def test_initiator_abort_txerror(config_file):

    if not ENABLE_THIS_TESTS:
      return 

    cfg = setUp(config_file, 'left')
    w3 = setup_provider(cfg.url, cfg.port, cfg.poa, cfg.ipc_path)
    
    token_instance = w3.eth.contract(abi=cfg.contract_abi, address=cfg.contract_address)
    (tokenId,cost) = await create_token(cfg.minter, token_instance, w3)
    assert token_instance.functions.getStateOfToken(tokenId).call() == 0
    await accept_token(cfg.minter, token_instance, w3, tokenId)
    assert token_instance.functions.getStateOfToken(tokenId).call() == 2
    await transfer_token(cfg.minter, token_instance, w3, tokenId)
    assert token_instance.functions.getStateOfToken(tokenId).call() == 1

    reason = 3
    

    ### Test Ethereum Initiator ###
    init = EthereumInitiator(cfg)
    init.timeout = 0
    result = await init.abort_sending(str(tokenId), reason)

    assert result["abort_status"] == False
    assert result["abort_error_code"] == ErrorCode.TIMEOUT


@pytest.mark.asyncio
async def test_initiator_commit_txerror(config_file):

    if not ENABLE_THIS_TESTS:
      return 
    cfg = setUp(config_file, 'left')
    w3 = setup_provider(cfg.url, cfg.port, cfg.poa, cfg.ipc_path)
    
    token_instance = w3.eth.contract(abi=cfg.contract_abi, address=cfg.contract_address)   
    (tokenId,cost) = await create_token(cfg.minter, token_instance, w3)
    assert token_instance.functions.getStateOfToken(tokenId).call() == 0
    await accept_token(cfg.minter, token_instance, w3, tokenId)
    assert token_instance.functions.getStateOfToken(tokenId).call() == 2
    await transfer_token(cfg.minter, token_instance, w3, tokenId)
    assert token_instance.functions.getStateOfToken(tokenId).call() == 1

    id = '2'

    ### Test Ethereum Initiator ###
    init = EthereumInitiator(cfg)
    init.timeout = 0
    result = await init.commit_sending(str(tokenId))

    assert result["commit_status"] == False
    assert result["commit_error_code"] == ErrorCode.TIMEOUT



@pytest.mark.asyncio
async def test_interledger_with_two_ethereum_abort__txerror(config_file):
    # set up ledgerA and ledgerB
    tokenId = uuid4().int
    cfg_A = setUp(config_file, 'left')
    cfg_B = setUp(config_file, 'right')

    w3_A = setup_provider(cfg_A.url, cfg_A.port, cfg_A.poa, cfg_A.ipc_path)
    w3_B = setup_provider(cfg_B.url, cfg_B.port, cfg_B.poa, cfg_B.ipc_path)
    token_instance_A = w3_A.eth.contract(abi=cfg_A.contract_abi, address=cfg_A.contract_address)
    token_instance_B = w3_B.eth.contract(abi=cfg_B.contract_abi, address=cfg_B.contract_address)
    
    await create_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
    await create_token(cfg_B.minter, token_instance_B, w3_B, tokenId)
    assert token_instance_B.functions.getStateOfToken(tokenId).call() == 0
    assert token_instance_A.functions.getStateOfToken(tokenId).call() == 0

    print("Test setup ready")

    with patch("interledger.interledger.Interledger.cleanup") as mock_cleanup:
        # mock cleanup to check the transfer reaches the end

        # Create interledger with a bridge in direction from A to B
        print("Building Interledger bridge A -> B")
        initiator = EthereumInitiator(cfg_A)
        responder = EthereumResponder(cfg_B)
        responder.timeout = 0
        interledger = Interledger(initiator, responder)

        print("Creating Intelredger run coroutine")
        interledger_task = asyncio.ensure_future(interledger.run())

        print("A smart contract call in ledger A marks the token in 'TransferOut'")

        # Activate token in ledgerA
        await accept_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 2
        await transfer_token(cfg_A.minter, token_instance_A, w3_A, tokenId)
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 1
       
        await asyncio.sleep(2) # Simulate Interledger running

        assert token_instance_B.functions.getStateOfToken(tokenId).call() == 2 # just collecting tx_receipt is failing not transaction itself
        assert token_instance_A.functions.getStateOfToken(tokenId).call() == 2
        assert len(interledger.transfers) == 1
        assert interledger.transfers[0].result["status"] == False
        assert interledger.transfers[0].result["error_code"] == ErrorCode.TIMEOUT


        tx_hash = interledger.transfers[0].result["tx_hash"]
        tx_info = w3_B.eth.getTransaction(tx_hash)
        assert tx_info ['blockHash'] != None

        assert len(interledger.results_commit) == 0
        assert len(interledger.results_abort) == 1

        tx_hash = interledger.results_abort[0]["abort_tx_hash"]
        status = interledger.results_abort[0]["abort_status"]
        tx_info = w3_A.eth.getTransaction(tx_hash)
        tx_receipt = w3_A.eth.getTransactionReceipt(tx_hash)
        # check also other information about the transaction
        assert tx_info ['hash'] == tx_hash
        assert tx_info ['blockHash'] != None
        assert tx_info ['to'] == cfg_A.contract_address
        #print(tx_info)
        # check function name and abi
        decoded_input = token_instance_A.decode_function_input(tx_info['input'])
        assert decoded_input[0].fn_name == token_instance_A.get_function_by_name("interledgerAbort").fn_name
        assert decoded_input[0].abi == token_instance_A.get_function_by_name("interledgerAbort").abi
        # check function parameters
        assert decoded_input[1]['id'] == tokenId
        assert decoded_input[1]['reason'] == 2
        print("Stopping Interledger run coroutine")
        interledger.stop()
        await interledger_task

        interledger.stop()
        await interledger_task
