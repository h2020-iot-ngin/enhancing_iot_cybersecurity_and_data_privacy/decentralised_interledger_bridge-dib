import { ethers } from "hardhat";
import "@nomiclabs/hardhat-waffle";
import { expect } from "chai";
import { Contract, Event, utils } from "ethers";


enum TransferState {
  NotApplicable = 0, // before set up, flag for non-existing transfer
  Initialized, // initial state when a data transfer is set up
  InitializedEndorsed, // Initialized gets endorsed by required consortium members
  Sent, // data is sent to destination
  Accepted, // data transfer is accepted by destination
  AcceptedEndorsed, // state Accepted gets endorsed by required consortium members
  Rejected, // data transfer is rejected by destination
  RejectedEndorsed, // state Rejected gets endorsed by required consortium members
  Confirming, // response above is fed back to source side
  Committed, // data transfer is committed
  CommittedEndorsed, // state Committed gets endorsed by required consortium members
  Aborted, // data transfer is aborted
  AbortedEndorsed, // state Aborted gets endorsed by required consortium members
  Declined // invalid action or misbehaving is detected
}

describe("Connection", function () {
  let connection: Contract;
  let owner: any;
  let bridges: any;
  let transfer: any;
  let endorseActionTx: any;
  let declineActionTx: any;
  let updateEntryTx: any;

  // 0.1 abort with invalid configs
  it("Should abort the connection creation with invalid configs", async () => {
    // // with zero required
    [owner, ...bridges] = await ethers.getSigners();
    const Connection = await ethers.getContractFactory("Connection");
    await expect(Connection.deploy(
      utils.toUtf8Bytes('{"endpoint": "es"}'), // placeholder for source endpoint information
      utils.toUtf8Bytes('{"endpoint": "ed"}'), // placeholder for source endpoint information
      [bridges[0].address, bridges[1].address, bridges[2].address, bridges[3].address], // allowed bridges
      0, // required, 0 is set here to be invalid on purpose
      5, 
      5, 
      12, 
      12
    )).to.be.reverted;
    console.log("Invalid configs results in contract creation aborted");
  });
  
  // 0.2 abort if required bridges > allowed bridges
  it("Should abort the connection creation if required bridges exceeds allowed bridges", async () => {
    [owner, ...bridges] = await ethers.getSigners();
    const Connection = await ethers.getContractFactory("Connection");
    await expect(Connection.deploy(
      utils.toUtf8Bytes('{"endpoint": "es"}'), // placeholder for source endpoint information
      utils.toUtf8Bytes('{"endpoint": "ed"}'), // placeholder for source endpoint information
      [bridges[0].address, bridges[1].address, bridges[2].address, bridges[3].address], // allowed bridges
      5, // required, 5 is set here to be invalid on purpose
      5, 
      5, 
      12, 
      12
    )).to.be.reverted;
  });

  // 0.3 create a connection
  it("Should create a connection properly", async () => {
    [owner, ...bridges] = await ethers.getSigners();
    console.log(`Owner address: ${owner.address}`);
    for (let i = 0; i <= 3; i++) {
      console.log(`Bridge ${i} address: ${bridges[i].address}`);
    }

    const Connection = await ethers.getContractFactory("Connection");
    connection = await Connection.deploy(
      utils.toUtf8Bytes('{"endpoint": "es"}'), // placeholder for source endpoint information
      utils.toUtf8Bytes('{"endpoint": "ed"}'), // placeholder for source endpoint information
      [bridges[0].address, bridges[1].address, bridges[2].address, bridges[3].address], // allowed bridges
      3, // required
      5, // reference timeout in seconds for source side
      5, // reference timeout in seconds for destination side
      12, // the upper limit of search over source ledger history when checking validity
      12 // the upper limit of search over destination ledger history when checking validity
    );
    await connection.deployed();

    console.log(`Connection contract deployed at ${connection.address}`);

    // check the connection parameters
    expect(await connection.requiredInstances()).to.equal(3);

    expect(await connection.timeoutSource()).to.equal(5);
    expect(await connection.timeoutDestination()).to.equal(5);
    expect(await connection.searchRangeSource()).to.equal(12);
    expect(await connection.searchRangeDestination()).to.equal(12);

    // check the roles
    expect(await connection.hasRole(
      utils.solidityKeccak256(["string"], ["OWNER"]),
      owner.address
    )).to.equal(true);

    for (let i = 0; i <= 3; i++) {
      expect(await connection.hasRole(
        utils.solidityKeccak256(["string"], ["ALLOWED_BRIDGE"]),
        bridges[i].address
      ));
    }

    // check endpoints information
    expect(await connection.getEndpointSource()).to.equal(
      '{"endpoint": "es"}'
    );

    expect(await connection.getEndpointDestination()).to.equal(
      '{"endpoint": "ed"}'
    );

    // check events
    const BridgeToSetup = connection.filters.BridgeToSetup();
    const logs = await connection.queryFilter(BridgeToSetup, -10, "latest");

    expect(logs[logs.length - 1].address).to.equal(connection.address);
  });
  
  // 1.1 create an entry mapping to an interledger transaction
  it("Should create an entry for a data transfer", async () => {
    const transferId = utils.formatBytes32String('101');
    const dataPayload = utils.toUtf8Bytes('0xdummy_data_1');
    const txHash = utils.formatBytes32String('');

    // create the entry with above information
    const createEntryTx = await connection
      .connect(bridges[0])
      .createEntry(
        transferId,
        dataPayload,
        0,
        txHash,
        0
    );
    await createEntryTx.wait();
    transfer = await connection.transfers(transferId);

    // check transfer entry created
    expect(transfer.id).to.equal(transferId);
    expect(utils.toUtf8String(transfer.data)).to.equal('0xdummy_data_1');

    // check events
    const EntryCreated = connection.filters.EntryCreated();
    const logs = await connection.queryFilter(EntryCreated, -10, "latest");
    expect(logs[logs.length - 1].transactionHash).to.equal(createEntryTx.hash);
  });

  // 1.2 multiple data transfers can be managed
  it("Should be able to create multiple entries", async () => {
    const transferId2 = utils.formatBytes32String('102');
    const dataPayload2 = utils.toUtf8Bytes('0xdummy_data_2');
    const txHash = utils.formatBytes32String('');
    
    const createEntryTx2 = await connection
      .connect(bridges[0])
      .createEntry(
        transferId2,
        dataPayload2,
        0,
        txHash,
        0
    );
    await createEntryTx2.wait();
    const transfer2 = await connection.transfers(transferId2);

    // check transfer entry created
    expect(transfer2.id).to.equal(transferId2);
    expect(utils.toUtf8String(transfer2.data)).to.equal('0xdummy_data_2');
  });
  
  
  // 1.3 should not allow creation of multiple transfer entries with the same id
  it("Should not create transfer entry if id is already in use", async () => {
    const transferId2 = utils.formatBytes32String('102');
    const dataPayload4 = utils.toUtf8Bytes('0xdummy_data_4');
    const txHash4 = utils.formatBytes32String('');

    const createEntryTx4 = await connection
      .connect(bridges[0])
      .createEntry(
        transferId2,
        dataPayload4,
        0,
        txHash4,
        0
    );
    
    await createEntryTx4.wait();
    const transfer4 = await connection.transfers(transferId2);
    // data should be the original: 0xdummy_data_2
    expect(utils.toUtf8String(transfer4.data)).not.to.equal('0xdummy_data_4');
  }); 
  

  // 1.4 invalidity is reported on initialization
  it("Should decline a transfer if invalidity is found", async () => {
    const transferId2 = utils.formatBytes32String('102');

    // we need to declines since we have 4 bridges and 3 of them are required for endorsement
    let declineActionTx1 = await connection
      .connect(bridges[1])
      .declineAction(
        transferId2,
        TransferState.Initialized
      )
    await declineActionTx1.wait();

    let declineActionTx2 = await connection
      .connect(bridges[2])
      .declineAction(
        transferId2,
        TransferState.Initialized
      )
    await declineActionTx2.wait();
    
    const transfer2 = await connection.transfers(transferId2);
    expect(transfer2.state).to.equal(TransferState.Declined);

    // check EntryUpdated event
    const EntryUpdated = connection.filters.EntryUpdated();
    const logs = await connection.queryFilter(EntryUpdated, -10, "latest");

    // check that event arguments and hash match
    expect(logs[logs.length - 1]?.args?.[0]).to.equal(transferId2);
    expect(logs[logs.length - 1]?.args?.[1]).to.equal(TransferState.Declined);
    expect(logs[logs.length - 1].transactionHash).to.equal(declineActionTx2.hash);
  });

  // 1.5 create invalid transfer
  it("Should not create transfer entry if bridge not allowed", async () => {
    const transferId3 = utils.formatBytes32String('102');
    const dataPayload3 = utils.toUtf8Bytes('0xdummy_data_3');

    await expect(connection
      .createEntry(
        transferId3,
        dataPayload3
    )).to.be.reverted;
  });
  


  // 2.1 transfer before ready
  it("Should have the transfer entry Initialized", async () => {
    const transferId = utils.formatBytes32String('101');

    console.log(`Transfer state: ${transfer.state}`);
    expect(transfer.state).to.equal(TransferState.Initialized);

    endorseActionTx = await connection
      .connect(bridges[1])
      .endorseAction(
        transferId,
        TransferState.Initialized
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.Initialized);
    expect(transfer.endorsed).to.equal(1);

    endorseActionTx = await connection
      .connect(bridges[2])
      .endorseAction(
        transferId,
        TransferState.Initialized
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.Initialized);
    expect(transfer.endorsed).to.equal(2);
  });

  // 2.2 transfer gets ready
  it("Should have the transfer entry initialization endorsed", async () => {
    const transferId = utils.formatBytes32String('101');

    endorseActionTx = await connection
      .connect(bridges[3])
      .endorseAction(
        transferId,
        TransferState.Initialized
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.InitializedEndorsed);
    expect(transfer.endorsed).to.equal(0);

    // check event of TransferReady => FIXME this...
    //const TransferReady = connection.filters.TransferReady();
    //const logs = await connection.queryFilter(TransferReady, -10, "latest");
    //expect(logs[logs.length - 1].transactionHash).to.equal(endorseActionTx.hash);
    
    // check EntryUpdated event
    const EntryUpdated = connection.filters.EntryUpdated();
    const logs = await connection.queryFilter(EntryUpdated, -10, "latest");

    // check that event arguments and hash match
    expect(logs[logs.length - 1]?.args?.[0]).to.equal(transferId);
    expect(logs[logs.length - 1]?.args?.[1]).to.equal(TransferState.InitializedEndorsed);
    expect(logs[logs.length - 1].transactionHash).to.equal(endorseActionTx.hash);
  });

  // 3.1 signal willingness to send the data transfer
  it("Should be able to signal willingness to send the data transfer", async () => {
    const transferId = utils.formatBytes32String('101');

    let willingSendTx = await connection
      .connect(bridges[1])
      .willingToSendTransfer(
        utils.formatBytes32String('101')
      );
    await willingSendTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      activeBridge: ${transfer.activeBridge}`);
    expect(transfer.state).to.equal(TransferState.Sent);
    expect(transfer.activeBridge).to.equal(bridges[1].address);

    // check event of EntryUpdated
    const EntryUpdated = connection.filters.EntryUpdated();
    const logs = await connection.queryFilter(EntryUpdated, -10, "latest");
    expect(logs[logs.length - 1].transactionHash).to.equal(willingSendTx.hash);
  });

  // 3.2 timeout if taken
  it("Should not change activeBridge if transfer is in Sent state", async () => {
    const transferId = utils.formatBytes32String('101');

    let willingSendTx = await connection
      .connect(bridges[2])
      .willingToSendTransfer(
        utils.formatBytes32String('101')
      );
    await willingSendTx.wait(); // return of false cannot retrieved off-chain

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      active bridge: ${transfer.activeBridge}`);
    expect(transfer.state).to.equal(TransferState.Sent);
    expect(transfer.activeBridge).not.to.equal(bridges[2].address);
  });
  
  // 4.1 non-active bridge tries to update
  it("Should revert request from non-active bridge", async () => {
    const transferId = utils.formatBytes32String('101');
    await expect(
      connection
        .connect(bridges[2])
        .updateEntry(
          transferId,
          TransferState.Rejected,
          1001
        )
      ).to.be.reverted;
  });
  
  // 4.2 should be able to update active bridge after the timeout has been reached
  it("Should be able to update active sending bridge after the timeout has been reached", async() => {
    await new Promise(resolve => setTimeout(resolve, 5000));
    
    const transferId = utils.formatBytes32String('101');
    
    let willingSendTx = await connection
      .connect(bridges[2])
      .willingToSendTransfer(
        utils.formatBytes32String('101')
      );
    await willingSendTx.wait();
    
    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      active bridge: ${transfer.activeBridge}`);
    expect(transfer.state).to.equal(TransferState.Sent);
    expect(transfer.activeBridge).to.equal(bridges[2].address);  
  });

  // 4.3 update entry with response
  it("Should update the entry with responder response", async () => {
    const transferId = utils.formatBytes32String('101');
    const txHash = utils.formatBytes32String('');

    updateEntryTx = await connection
      .connect(bridges[2])
      .updateEntry(
        transferId,
        TransferState.Accepted,
        1001,
        0,
        txHash,
        0
      );
    await updateEntryTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      active bridge: ${transfer.activeBridge},
      nonce: ${transfer.nonce}`);
    expect(transfer.state).to.equal(TransferState.Accepted);
    expect(transfer.nonce).to.equal(1001);
  });

  // 4.4 endorse the response
  it("Should have the transfer entry response endorsed", async () => {
    const transferId = utils.formatBytes32String('101');

    endorseActionTx = await connection
      .connect(bridges[1])
      .endorseAction(
        transferId,
        TransferState.Accepted
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.Accepted);
    expect(transfer.endorsed).to.equal(1);

    endorseActionTx = await connection
      .connect(bridges[2])
      .endorseAction(
        transferId,
        TransferState.Accepted
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.Accepted);
    expect(transfer.endorsed).to.equal(2);

    endorseActionTx = await connection
      .connect(bridges[3])
      .endorseAction(
        transferId,
        TransferState.Accepted
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.AcceptedEndorsed);
    expect(transfer.endorsed).to.equal(0);

    // check EntryUpdated event
    const EntryUpdated = connection.filters.EntryUpdated();
    const logs = await connection.queryFilter(EntryUpdated, -10, "latest");

    // check that event arguments and hash match
    expect(logs[logs.length - 1]?.args?.[0]).to.equal(transferId);
    expect(logs[logs.length - 1]?.args?.[1]).to.equal(TransferState.AcceptedEndorsed);
    expect(logs[logs.length - 1].transactionHash).to.equal(endorseActionTx.hash);
  });

  // 4.5 exceeding endorsement
  it("Should revert additional endorse if already endorsed", async () => {
    const transferId = utils.formatBytes32String('101');

    await expect(connection
      .connect(bridges[0])
      .endorseAction(
        transferId,
        TransferState.Accepted
      )).to.be.reverted;
  });

  // 5.1 signal willingness to finalize transfer
  it("Should be able to signal willingness to finalize the data transfer", async () => {
    const transferId = utils.formatBytes32String('101');

    let willingFinalizeTx = await connection
      .connect(bridges[2])
      .willingToFinalizeTransfer(
        utils.formatBytes32String('101')
      );
    await willingFinalizeTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      activeBridge: ${transfer.activeBridge}`);
    expect(transfer.state).to.equal(TransferState.Confirming);
    expect(transfer.activeBridge).to.equal(bridges[2].address);

    // check event of EntryUpdated
    const EntryUpdated = connection.filters.EntryUpdated();
    const logs = await connection.queryFilter(EntryUpdated, -10, "latest");
    expect(logs[logs.length - 1].transactionHash).to.equal(willingFinalizeTx.hash);
  });

  // 5.2 handle late signal
  it("Should not change activeBridge if transfer is in Confirming state", async () => {
    const transferId = utils.formatBytes32String('101');

    transfer = await connection.transfers(transferId);
    //console.log(transfer);

    let willingFinalizeTx = await connection
      .connect(bridges[3])
      .willingToFinalizeTransfer(
        utils.formatBytes32String('101')
      );
    await willingFinalizeTx.wait(); // return of false cannot retrieved off-chain

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      active bridge: ${transfer.activeBridge}`);
    expect(transfer.state).to.equal(TransferState.Confirming);
    expect(transfer.activeBridge).not.to.equal(bridges[3].address);
  });

  // 6.1 non-active bridge tries to update
  it("Should revert request from non-active bridge", async () => {
    const transferId = utils.formatBytes32String('101');
    await expect(
      connection
        .connect(bridges[3])
        .updateEntry(
          transferId,
          TransferState.Committed,
          0
        )
      ).to.be.reverted;
  });
  
  // 6.2 should be able to update active bridge after the timeout has been reached
  it("Should be able to update active confirming bridge after the timeout has been reached", async() => {
    await new Promise(resolve => setTimeout(resolve, 5000));
    
    const transferId = utils.formatBytes32String('101');
    
    let willingFinalizeTx = await connection
      .connect(bridges[1])
      .willingToFinalizeTransfer(
        utils.formatBytes32String('101')
      );
    await willingFinalizeTx.wait();
    
    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      active bridge: ${transfer.activeBridge}`);
    expect(transfer.state).to.equal(TransferState.Confirming);
    expect(transfer.activeBridge).to.equal(bridges[1].address);  
  });

  // 6.3 update entry with confirm
  it("Should update the entry with initiator confirming feedback", async () => {
    const transferId = utils.formatBytes32String('101');
    const txHash = utils.formatBytes32String('');
    
    updateEntryTx = await connection
      .connect(bridges[1])
      .updateEntry(
        transferId,
        TransferState.Committed,
        0, // does not matter when confirming
        0,
        txHash,
        0
      );
    await updateEntryTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      active bridge: ${transfer.activeBridge}`);
    expect(transfer.state).to.equal(TransferState.Committed);
  });

  // 7.1 endorse confirm
  it("Should have the entry confirming endorsed", async () => {
    const transferId = utils.formatBytes32String('101');

    endorseActionTx = await connection
      .connect(bridges[1])
      .endorseAction(
        transferId,
        TransferState.Committed
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.Committed);
    expect(transfer.endorsed).to.equal(1);

    endorseActionTx = await connection
      .connect(bridges[2])
      .endorseAction(
        transferId,
        TransferState.Committed
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.Committed);
    expect(transfer.endorsed).to.equal(2);

    endorseActionTx = await connection
      .connect(bridges[3])
      .endorseAction(
        transferId,
        TransferState.Committed
      );
    await endorseActionTx.wait();

    transfer = await connection.transfers(transferId);
    console.log(`Transfer state: ${transfer.state}, 
      endorsed by: ${transfer.endorsed}`);
    expect(transfer.state).to.equal(TransferState.CommittedEndorsed);
    expect(transfer.endorsed).to.equal(0);

    // check event of EntryUpdated
    const EntryUpdated = connection.filters.EntryUpdated();
    const logs = await connection.queryFilter(EntryUpdated, -10, "latest");
    expect(logs[logs.length - 1].transactionHash).to.equal(endorseActionTx.hash);
    console.log("Interledger data transfer is successfully finalized.");
  });

});
