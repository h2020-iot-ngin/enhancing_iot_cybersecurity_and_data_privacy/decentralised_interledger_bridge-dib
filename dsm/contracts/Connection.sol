// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/access/AccessControl.sol";

import "hardhat/console.sol";

/** 
 * @title Connection
 * @dev Implements a connection between DLT endpoints, for 
 * decentralized interledger transactions (data transfers) 
 * with shared trust among a consortium
 */
contract Connection is AccessControl {
    /**
     * Types
     */
    enum TransferState {
        NotApplicable, // before set up, flag for non-existing transfer
        Initialized, // initial state when a data transfer is set up
        InitializedEndorsed, // Initialized gets endorsed by required consortium members
        Sent, // data is sent to destination
        Accepted, // data transfer is accepted by destination
        AcceptedEndorsed, // state Accepted gets endorsed by required consortium members
        Rejected, // data transfer is rejected by destination
        RejectedEndorsed, // state Rejected gets endorsed by required consortium members
        Confirming, // response above is fed back to source side
        Committed, // data transfer is committed
        CommittedEndorsed, // state Committed gets endorsed by required consortium members
        Aborted, // data transfer is aborted
        AbortedEndorsed, // state Aborted gets endorsed by required consortium members
        Declined // invalid action or misbehaving is detected
    }
    
    enum EndorsementState {
        Unchecked, // initial state, with no endorsement at all
        TransferReadyChecked, // 
        TransferResponseChecked,
        TransferConfirmChecked
    }

    /**
     * Stores information about client ledger event or transaction, this information 
     * is used for verifying Interledger operations
     */
    struct Transaction {
        uint256 blockNumber; // block number of the transaction that produced the event
        bytes32 transactionHash; // hash of the transaction that produced the event
        uint256 additionalData; // either index of the event within the transaction or reason for aborting
    }
    
    struct Transfer {
        uint256 id; // transfer id
        bytes data; // data payload of a transfer
        TransferState state; // state of a data transfer
        address activeBridge; // active bridge instance to send the transfer or process response
        uint endorsed; // number of endorsements for the current state
        uint nonce; // unique identifer for action to destination side
        Transaction initialTx; // details about initial transaction that triggers the Interledger process
        Transaction responderTx; // details about transaction on Responder side
        Transaction finalTx; // details about the final transaction that concludes the Interledger process
        uint rejected; // number of rejections for the current state
        uint256 willingToSendTimestamp; // time when node indicated willingness to send transfer
        uint256 willingToFinalizeTimestamp; // time when node indicated willingness to finalize transfer
    }
    
    /**
     * Fields
     */    
    bytes public endpointSource; // a JSON string to describe the information of endpoint at sourc side
    bytes public endpointDestination; // a JSON string to describe the information of endpint at destination side

    address[] public allowedBridges; // an array of Interledger bridges that can participate in the process
    
    uint public requiredInstances; // an integer number to indicate the lower bound of required bridges to conduct and endorse the transfer
    
    uint public timeoutSource; // a number as reference timeout in seconds for source side
    uint public timeoutDestination; // a number as reference timeout in seconds for destination side
    
    uint public searchRangeSource; // a number to indicate the upper limit of search over source ledger history when checking validity
    uint public searchRangeDestination; // a number to indicate the upper limit of search over destination ledger history when checking validity
    
    mapping(uint256 => Transfer) public transfers; // a mapping from a transfer id to the transfer entry
    
    // fields below are needed to prevent nodes from voting twice
    // a record of endorsement state of each transfer by each bridge
    mapping(uint256 => mapping(address => EndorsementState)) endorsements; 

    // a record of rejection state of each transfer by each bridge
    mapping(uint256 => mapping(address => EndorsementState)) rejections;

    
    /**
     * Roles
     */
    // the owner of the connection contract
    bytes32 public constant OWNER = keccak256("OWNER"); 
    
    // allowed addresses for bridge instances controlled by consortium participants
    bytes32 public constant ALLOWED_BRIDGE = keccak256("ALLOWED_BRIDGE"); 

    
    /**
     * Events
     */
    // event for setting up bridges
    event BridgeToSetup(bytes indexed endpointSource, bytes indexed endpointDestination);
    
    // event for the creation of a transfer entry
    event EntryCreated(uint256 indexed id, bytes indexed data);
    
    // event for the update of a transfer entry
    event EntryUpdated(uint256 indexed id, TransferState indexed state);

    
    /** 
     * Errors
     */
    error NotApplicableTransfer(uint256 transferId, address bridge);
    error NotAllowedBridge(uint256 transferId, address bridge);
    error InactiveBridgeToUpdate(uint256 transferId, address bridge);
    error InvalidEndorsement(uint256 transferId, address bridge, TransferState state);
    error InvalidDecline(uint256 transferId, address bridge, TransferState state);
    error InvalidSignal(uint256 tranferId, address bridge, TransferState state);
    
    /**
     * Modifiers
     */
    modifier onlyAllowedBridge(uint256 transferId) {
        if (!hasRole(ALLOWED_BRIDGE, msg.sender)) {
            revert NotAllowedBridge(transferId, msg.sender);
        }
        _;
    }
    
    modifier onlyActiveBridge(uint256 transferId) {
        if (msg.sender != transfers[transferId].activeBridge) {
            revert InactiveBridgeToUpdate(transferId, msg.sender);
        }
        _;
    }
    
    /**
     * Methods
     */
    
    /**
     * @dev Set up state mangement for a connection between specific endpoints
     * @param es a JSON string to describe the information of endpoint at source side
     * @param ed a JSON string to describe the information of endpoint at destination side
     * @param allowed an array of allowed addresses for bridge instances controlled by consortium participants
     * @param required an integer number to indicate the lower bound of required bridges to conduct and endorse the transfer
     * @param ts a number as reference timeout in seconds for source side
     * @param td a number as reference timeout in seconds for destination side
     * @param srs a number to indicate the upper limit of search over source ledger history when checking validity
     * @param srd a number to indicate the upper limit of search over destination ledger history when checking validity
     */
    constructor(
        bytes memory es, 
        bytes memory ed, 
        address[] memory allowed, 
        uint required,
        uint ts,
        uint td,
        uint srs,
        uint srd
    ) {
        // setup connection configs
        require(
            required > 0, 
            "At least oneself is required."
        );
        
        require(
            allowed.length >= required,
            "number of allowed bridges should be equal or bigger than required instances"
        );
        requiredInstances = required;
        console.log("Required bridge instances:", requiredInstances);
        
        require(
            ts > 0 && td > 0 && srs > 0 && srd > 0,
            "Postive values expected for timeouts and search range of ledger history"
        );
        timeoutSource = ts;
        timeoutDestination = td;
        console.log("Timeout at source:", timeoutSource);
        console.log("Timeout at destination:", timeoutDestination);
        searchRangeSource = srs;
        searchRangeDestination = srd;
        console.log("Search range at source:", searchRangeSource);
        console.log("Search range at destination:", searchRangeDestination);

        // set up roles
        _setupRole(OWNER, msg.sender);
        
        require(
            allowed.length > 0,
            "At least one bridge should be able to transfer"
        );

        for (uint i = 0; i < allowed.length; i++) {
            _setupRole(ALLOWED_BRIDGE, allowed[i]);
        }
        
        // store allowed bridges, necessary for reusing transfer ids
        allowedBridges = allowed;
        
        // set endpoints for connection
        endpointSource = es;
        endpointDestination = ed;
        
        emit BridgeToSetup(endpointSource, endpointDestination);
    }

    /**
     * @dev Get the source endpoint information as a JSON string
     * @return string for source endpoint
     */
    function getEndpointSource() public view returns (string memory) {
        return string(endpointSource);
    }

    /**
     * @dev Get the destination endpoint information as a JSON string
     * @return string for destination endpoint
     */
    function getEndpointDestination() public view returns (string memory) {
        return string(endpointDestination);
    }
    
    /**
     * @dev Create a transfer entry corresponding to the specified id and data payload
     * @param id transfer id
     * @param data data payload of a transfer
     * @param blockNumber block number of the transaction that produced the event
     * @param transactionHash hash of the transaction
     * @param logIndex index of the event within the transaction
     * @return entry created or not
     */
    function createEntry(
        uint256 id,
        bytes calldata data,
        uint256 blockNumber,
        bytes32 transactionHash,
        uint256 logIndex
    ) public onlyAllowedBridge(id) returns (bool)  {
        // check if entry exists, also allow reusing id for transfers that
        // have been concluded
        if ((transfers[id].state != TransferState.NotApplicable) &&
            (transfers[id].state != TransferState.CommittedEndorsed) &&
            (transfers[id].state != TransferState.AbortedEndorsed) &&
            (transfers[id].state != TransferState.Declined)) {
            return false;
        }
        
        // reset endorsements and rejections if we are reusing transfer ids
        if (transfers[id].state != TransferState.NotApplicable) {
            for (uint i = 0; i < allowedBridges.length; i++) {
                endorsements[id][allowedBridges[i]] = EndorsementState.Unchecked;
                rejections[id][allowedBridges[i]] = EndorsementState.Unchecked;
            }
        }
        
        // create a new transfer entry
        transfers[id] = Transfer({
            id: id,
            data: data,
            state: TransferState.Initialized,
            activeBridge: 0x0000000000000000000000000000000000000000,
            endorsed: 0,
            nonce: 0,
            initialTx: Transaction(blockNumber, transactionHash, logIndex),
            responderTx: Transaction(0, 0, 0),
            finalTx: Transaction(0, 0, 0),
            rejected: 0,
            willingToSendTimestamp: 0,
            willingToFinalizeTimestamp: 0
        });
        
        emit EntryCreated(id, data);
        return true;
    }
    
    /**
     * @dev Update the state of a transfer entry corresponding to the specified id
     * blockNumber, transactionHash, and additionalData parameters are only used when updating
     * state to Accepted, Rejected, Committed, Aborted
     * @param id transfer id
     * @param state state of transfer
     * @param nonce unique identifer for action to destination side, for other cases 0 can be used
     * @param blockNumber block number of the transaction that produced the event
     * @param transactionHash hash of the transaction
     * @param additionalData either index of the event within the transaction or reason for aborting
     * @return entry updated or not
     */
    function updateEntry(
        uint256 id, 
        TransferState state, 
        uint nonce,
        uint256 blockNumber,
        bytes32 transactionHash,
        uint256 additionalData
    ) public onlyAllowedBridge(id) returns (bool) {
        // update transfer from sent
        if (state == TransferState.Accepted || state == TransferState.Rejected) {
            require(
                transfers[id].state == TransferState.Sent,
                "Only transfer sent can be accepted or rejected"
            );
            _updateSent(id, state, nonce, blockNumber, transactionHash, additionalData);
            return true;
        }
        
        // update tranfer from confirming
        if (state == TransferState.Committed || state == TransferState.Aborted) {
            require(
                transfers[id].state == TransferState.Confirming,
                "Only transfer confirming can be committed or aborted"
            );
            _updateConfirming(id, state, blockNumber, transactionHash, additionalData);
            return true;
        }
        return false;
    }
    
    /**
     * @dev utility function to update a transfer that has been sent to responder
     * @param id transfer id
     * @param state state of transfer
     * @param nonce unique identifer for action to destination side
     * @param blockNumber block number of the transaction that produced the event
     * @param transactionHash hash of the transaction
     * @param logIndex index of the event within the transaction
     */
    function _updateSent(
        uint256 id, 
        TransferState state, 
        uint nonce,
        uint256 blockNumber,
        bytes32 transactionHash,
        uint256 logIndex
    ) private onlyActiveBridge(id) {
        
        _updateTransferState(id, state);
        transfers[id].nonce = nonce;
        transfers[id].responderTx = Transaction(blockNumber, transactionHash, logIndex);
    }
    
    /** 
     * @dev utility function to update a confirming transfer
     * @param id transfer id
     * @param state state of transfer
     * @param blockNumber block number of the transaction
     * @param transactionHash hash of the transaction
     * @param reason reason for aborting (0 if transfer has been accepted)
     */
    function _updateConfirming(
        uint256 id, 
        TransferState state,
        uint256 blockNumber,
        bytes32 transactionHash,
        uint256 reason
    ) private onlyActiveBridge(id) {
        /*require(
            msg.sender == transfers[id].activeBridge,
            "Only the active bridge can update a transfer"
        );*/

        transfers[id].finalTx = Transaction(blockNumber, transactionHash, reason);
        _updateTransferState(id, state);
    }
    
    /**
     * @dev utility to update transfer state Only
     * @param id transfer id
     * @param state state of transfer
     */
    function _updateTransferState(
        uint256 id,
        TransferState state
    ) private {
        Transfer storage transfer = transfers[id];
        if (transfer.state == TransferState.NotApplicable) {
            revert NotApplicableTransfer(id, msg.sender);
        }
        
        transfer.state = state;
        transfer.activeBridge = 0x0000000000000000000000000000000000000000; // reset
        transfer.endorsed = 0; // reset
        
        emit EntryUpdated(id, state);
    }
    
    /**
     * @dev endorse an action that has already been taken and recorded as valid
     * @param id transfer id
     * @param state state of transfer
     * @return the endorsement succeed or not
     */
    function endorseAction(
        uint256 id, 
        TransferState state
    ) public onlyAllowedBridge(id) returns (bool) {
        // check endorsment validity
        if (state != TransferState.Initialized &&
            state != TransferState.Accepted &&
            state != TransferState.Rejected &&
            state != TransferState.Committed &&
            state != TransferState.Aborted) {
            revert InvalidEndorsement(id, msg.sender, state);
        }
        
        Transfer storage transfer = transfers[id];
        if (transfer.state != state) {
            revert InvalidEndorsement(id, msg.sender, state);
        }
        
        // endorse
        EndorsementState es = endorsements[id][msg.sender];
        
        if (state == TransferState.Initialized) {
            if (es >= EndorsementState.TransferReadyChecked) {
                revert InvalidEndorsement(id, msg.sender, state);      
            }

            endorsements[id][msg.sender] = EndorsementState.TransferReadyChecked;
            transfer.endorsed += 1;
            
            if (transfer.endorsed == requiredInstances) {
                _updateTransferState(id, TransferState.InitializedEndorsed);
                
                //emit TransferReady(id);
            }
            return true;
        }
        
        if (state == TransferState.Accepted ||
            state == TransferState.Rejected) {
            if (es >= EndorsementState.TransferResponseChecked) {
                revert InvalidEndorsement(id, msg.sender, state);  
            }
            endorsements[id][msg.sender] = EndorsementState.TransferResponseChecked;
            transfer.endorsed += 1;
            
            if (transfer.endorsed == requiredInstances) {
                if (state == TransferState.Accepted) {
                    _updateTransferState(id, TransferState.AcceptedEndorsed); 
                } else {
                    _updateTransferState(id, TransferState.RejectedEndorsed);
                }
                
                //emit TransferResponded(id);
            }
            return true;
        }
        
        if (state == TransferState.Committed ||
            state == TransferState.Aborted) {
            if (es >= EndorsementState.TransferConfirmChecked) {
                revert InvalidEndorsement(id, msg.sender, state);   
            }
            endorsements[id][msg.sender] = EndorsementState.TransferConfirmChecked;
            transfer.endorsed += 1;

            if (transfer.endorsed == requiredInstances) {
                if (state == TransferState.Committed) {
                    _updateTransferState(id, TransferState.CommittedEndorsed);
                } else {
                    _updateTransferState(id, TransferState.AbortedEndorsed);
                }
            }
            return true;
        }
        
        return false;
    }
    
    /**
     * @dev decline an action due to misbehaving
     * @param id transfer id
     * @param state the state of the TransferState,
     * @return the decline succeed or not
     */
    function declineAction(
        uint256 id, 
        TransferState state
    ) public onlyAllowedBridge(id) returns (bool) {
        // check decline validity
        if (state == TransferState.NotApplicable ||
            state == TransferState.Sent ||
            state == TransferState.Confirming) {
            revert InvalidDecline(id, msg.sender, state);
        }

        Transfer storage transfer = transfers[id];
        if (transfer.state != state) {
            revert InvalidDecline(id, msg.sender, state);
        }    

        // decline
        EndorsementState es = rejections[id][msg.sender];
        
        if (state == TransferState.Initialized) {
            if (es >= EndorsementState.TransferReadyChecked) {
                revert InvalidDecline(id, msg.sender, state);      
            }

            rejections[id][msg.sender] = EndorsementState.TransferReadyChecked;
            transfer.rejected += 1;

            // check number of rejections, if transfer can't move to endorsed state anymore,
            // move it to the Declined state
            if (transfer.rejected == (allowedBridges.length - requiredInstances + 1)) {
                _updateTransferState(id, TransferState.Declined);    
            }
            return true;
        }
        
        if (state == TransferState.Accepted ||
            state == TransferState.Rejected) {
            if (es >= EndorsementState.TransferResponseChecked) {
                revert InvalidDecline(id, msg.sender, state);  
            }
            rejections[id][msg.sender] = EndorsementState.TransferResponseChecked;
            transfer.rejected += 1;
            
            if (transfer.rejected == (allowedBridges.length - requiredInstances + 1)) {
                _updateTransferState(id, TransferState.Declined);    
            }
            return true;
        }        
        
        if (state == TransferState.Committed ||
            state == TransferState.Aborted) {
            if (es >= EndorsementState.TransferConfirmChecked) {
                revert InvalidDecline(id, msg.sender, state);   
            }
            rejections[id][msg.sender] = EndorsementState.TransferConfirmChecked;
            transfer.rejected += 1;

            if (transfer.rejected == (allowedBridges.length - requiredInstances + 1)) {
                _updateTransferState(id, TransferState.Declined);    
            }
            return true;
        }
        
        return false;
    }
    
    /**
     * @dev Indicated willingness to send the IL transfer to the responder
     * @param id transfer id
     * @return signal of accept succeed or not
     */
    function willingToSendTransfer(
        uint256 id
    ) public onlyAllowedBridge(id) returns (bool) {
        // check validity to signal    
        Transfer storage transfer = transfers[id];

        if (transfer.state == TransferState.Sent) {
            // check wherever the timeout has been reached
            if (block.timestamp > (transfer.willingToSendTimestamp + timeoutDestination)) {
                // update the transfer details
                console.log("Updating transfer (Sent)", id, 
                            "with new active bridge", msg.sender);
                _updateTransferState(id, TransferState.Sent);
                transfer.willingToSendTimestamp = block.timestamp;
                transfer.activeBridge = msg.sender;
                return true;
            } else {
                return false; // signaled by other already and timeout hasn't been reached
            }
        }
        
        if (transfer.state != TransferState.InitializedEndorsed) {
            revert InvalidSignal(id, msg.sender, TransferState.Sent);
        }
        
        // signal & update transfer state
        _updateTransferState(id, TransferState.Sent);
        transfer.willingToSendTimestamp = block.timestamp;
        transfer.activeBridge = msg.sender;
        return true;
    }
    
    /**
     * @dev Signal the accept to process the response/result back at the source side
     * @param id transfer id
     * @return signal of accept succeed or not
     */
    function willingToFinalizeTransfer(
        uint256 id
    ) public onlyAllowedBridge(id) returns (bool) {
        // check validity to signal    
        Transfer storage transfer = transfers[id];

        if (transfer.state == TransferState.Confirming) {
            // check wherever the timeout has been reached
            if (block.timestamp > (transfer.willingToFinalizeTimestamp + timeoutSource)) {
                // update the transfer details
                console.log("Updating transfer (Confirming)", id, 
                            "with new active bridge", msg.sender);
                _updateTransferState(id, TransferState.Confirming);
                transfer.willingToFinalizeTimestamp = block.timestamp;
                transfer.activeBridge = msg.sender;
                return true;
            } else {
                return false; // signaled by other already
            }
        }
        
        if (transfer.state != TransferState.AcceptedEndorsed &&
            transfer.state != TransferState.RejectedEndorsed) {
            revert InvalidSignal(id, msg.sender, TransferState.Sent);
        }
        
        // signal & update transfer state
        _updateTransferState(id, TransferState.Confirming);
        transfer.willingToFinalizeTimestamp = block.timestamp;
        transfer.activeBridge = msg.sender;
        return true;
    }
}
