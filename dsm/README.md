# Decentralized State Management layer

The work in this directory is to develop and test the logic of Decentralized State Managment (DSM) layer of interledger transactions, or data transfer across ledgers interchangably.

The smart contract of the connection logic is implemented for Ethereum without losing generality, which can found at `./contracts/Connection.sol`. 

## Unit tests

Dependencies should be installed before the unit testing, using `npm install`.

Under this directory, run the following to conduct the unit test of the logic.

```
npm run test
```

or

```
npx hardhat test
```
