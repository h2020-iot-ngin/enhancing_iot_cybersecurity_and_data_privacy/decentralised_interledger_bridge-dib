const fs = require("fs");
const path = require("path");
const ini = require("ini");
const { utils } = require("ethers");

const Migrations = artifacts.require("Migrations");
const Connection = artifacts.require("Connection");

/**
 * @dev Migration script for DIB
 * Updates configuration with details for DSM ledgers, should be run after after Initiator/Responder
 * migrations.
 * Note: reads and modifies configuration files using base file name given as "--config_file" argument: 
 *  e.g., dib-node-1.cfg, dib-node-2.cfg, etc.
 * all of these configuration files must already exist, this script doesn't modify the base file directly
 * @param --nodes       number of nodes in the DSM ledgers (default: 1)
 * @param --config_file path to base config file to be used for deployment
 *                      (default: ../configs/dib.cfg)
 */

// parse custom arguments
const argv = require('minimist')(process.argv.slice(2));

/**
 * @dev Helper function for deploying the contract
 * @param   deployer    deployer to use
 * @param   Connection  Connection
 * @param   source_endpoint endpoint information of the source ledger as JSON string
 * @param   destination_endpoint endpoint information of the destination ledger as JSON string
 * @param   allowed_nodes   array of nodes that are allowed to participate in DSM
 * @param   endorsements    lower bound for number of nodes that are required to endorse transfers
 * @param   source_timeout  timeout in seconds on source side  
 * @param   destination_timeout timeout in seconds on destination side
 * @param   source_search_history upper limit for searching history on source side
 * @param   destination_search_history  upper limit for searching history on destination side
 * @param   owner       owner of the DSM smart contract
 */ 
async function deploy_contract(deployer, Connection, source_endpoint, destination_endpoint, 
                               allowed_nodes, endorsements, source_timeout, destination_timeout,
                               source_search_history, destination_search_history, owner) {
  const connection = await deployer.deploy(Connection, utils.toUtf8Bytes(source_endpoint),
                                           utils.toUtf8Bytes(destination_endpoint), allowed_nodes, 
                                           endorsements, source_timeout, destination_timeout, source_search_history, destination_search_history, {'from': owner});    
  return connection;
}


module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    if ((network == "dsm_ledger") || (network == "dsm_compose")) {
      await deployer.deploy(Migrations);
    
      const base_path = "dsm/contracts/";
      let nodes, config_file_path;
      
      // check --nodes argument
      if (Number.isFinite(argv['nodes'])) {
        if (argv['nodes'] <= accounts.length) {
          nodes = argv['nodes'];
        } else {
          nodes = accounts.length;
          console.log("Warning: not enought available accounts for all nodes, limiting number of nodes to",
                      nodes);
        }
      } else {
        console.log("Warning: Nodes parameter missing or invalid, defaulting to 1");
        nodes = 1;
      }
      
      // majority of nodes should be required for endorsing DSM transfers
      let endorsements = Math.ceil((nodes + 1) / 2);
      
      let alice = accounts[0];
      let allowed_nodes = accounts.slice(0, nodes); // nodes that are allowed to participate in DSM

      // get path to the configuration file
      if (typeof(argv['config_file']) != 'undefined') {
          config_file_path = argv['config_file'];
      } else {
          config_file_path = '../configs/dib.cfg';
      }

      
      // loop through all configuration files
      let new_config_file, extension, config, direction;
      let connection1 = null, connection2 = null, dsm1, dsm2;
      for (i = 1; i <= nodes; i++) {
        if (path.extname(config_file_path)) { // check for config file extension
          extension = path.extname(config_file_path);
          new_config_file = path.dirname(config_file_path) + "/" + 
                                path.basename(config_file_path, extension) + "-node-" + i + extension;
        } else { // no file extension
          new_config_file = path.dirname(config_file_path) + "/" + 
                                path.basename(config_file_path) + "-node-" + i;
        }
        
        if (!fs.existsSync(new_config_file)) {
          console.log("Error: file %s doesn't exist\n", new_config_file);
          return;
        }

        config = ini.parse(fs.readFileSync(new_config_file, 'utf-8'));

        // in a case of first configuration file, read details about Initiator and 
        // Responder ledgers, and deploy DSM contract(s)
        if (i == 1) {
          // parse details of right and left ledger, these will be given to 
          // the deployed DSM contract
          const left = config['service']['left'];
          const left_ledger = {'type': config[left].type, 'url': config[left].url, 
              'port': config[left].port, 'contract': config[left].contract};
      
          const right = config['service']['right'];
          const right_ledger = {'type': config[right].type, 'url': config[right].url, 
              'port': config[right].port, 'contract': config[right].contract};
              
          const direction = config['service']['direction'];
          
          dsm1 = config['service']['dsm1'];

          // check timeouts
          if (config[dsm1].source_timeout) {
            source_timeout = config[dsm1].source_timeout;
          } else {
            source_timeout = 10;
          }
          
          if (config[dsm1].destination_timeout) {
            destination_timeout = config[dsm1].destination_timeout;
          } else {
            destination_timeout = 10;
          }
          
          if (direction == 'both') { // deploy two contracts
            connection1 = await deploy_contract(deployer, Connection, left_ledger,
                                                right_ledger, allowed_nodes, endorsements,
                                                source_timeout, destination_timeout, 12, 12,
                                                alice);
            connection2 = await deploy_contract(deployer, Connection, right_ledger,
                                                left_ledger, allowed_nodes, endorsements,
                                                source_timeout, destination_timeout, 12, 12,
                                                alice);
          } else if (direction == 'left-to-right') { 
            connection1 = await deploy_contract(deployer, Connection, left_ledger,
                                                right_ledger, allowed_nodes, endorsements,
                                                source_timeout, destination_timeout, 12, 12,
                                                alice);
          } else if (direction == 'right-to-left') {
            connection1 = await deploy_contract(deployer, Connection, right_ledger,
                                                left_ledger, allowed_nodes, endorsements,
                                                source_timeout, destination_timeout, 12, 12,
                                                alice);
          } else { // invalid direction
              console.log("Error: Invalid 'direction' under 'service', should: both, left-to-right, or right-to-left");
              return;
          }
        }
        
        // for all nodes: update configuration file
        config[dsm1].contract_abi = base_path + "Connection.abi.json";
        config[dsm1].contract = connection1.address;
        config[dsm1].minter = allowed_nodes[i-1];
        config[dsm1].source_timeout = source_timeout;
        config[dsm1].destination_timeout = destination_timeout;
        
        if (connection2 !== null) { // add also details for DSM2 ledger if necessary
          dsm2 = config['service']['dsm2'];
          config[dsm2].contract_abi = base_path + "Connection.abi.json";
          config[dsm2].contract = connection2.address;
          config[dsm2].minter = allowed_nodes[i-1];
          config[dsm2].source_timeout = source_timeout;
          config[dsm2].destination_timeout = destination_timeout;
        }
        
        // write configuration file
        fs.writeFileSync(new_config_file, ini.stringify(config));
      }
      
      return;
    }
    console.log("Error: Wrong network given, should be: dsm_ledger");
  });
};
