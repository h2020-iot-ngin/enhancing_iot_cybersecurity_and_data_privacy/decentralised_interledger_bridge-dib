# DIB Interledger Component

## Table of Contents
- [Description](#description)
  * [Architecture Overview](#architecture-overview)
  * [Key Technologies](#key-technologies)
- [Usage](#usage)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
  * [Configuration](#configuration)
  * [Execution](#execution)
  * [Docker Images](#docker-images)
- [Testing](#testing)
  * [Prerequisites for Testing](#prerequisites-for-testing)
  * [Running the Tests](#running-the-tests)
  * [Evaluating the Results](#evaluating-the-results)
  * [Test for multi-ledgers-transaction](#test-for-multi-ledgers-transaction)
- [Generating Documentation](#generating-documentation)
  * [Requirements](#requirements)
  * [Generation](#generation)
  * [Miscs](#miscs)
- [Open Issues](#open-issues)
- [Future Work](#future-work)
- [Release Notes](#release-notes)
- [Contact Information](#contact-information)
- [License](#license)

## Description

This is the Decentralized Interledger Bridge (DIB) Interledger component, which originates from its single-node version Flexible Interledger Bridge (FIB) interledger in the [SOFIE Framework](https://github.com/SOFIE-project/Framework).

DIB Interledger enables atomic general-purpose data transfer between distributed ledgers, just as FIB without requiring any changes to the ledgers themselves. The Interledger component enables activity on an *Initiator* ledger to trigger activity on one or more *Responder* ledgers in an atomic manner. The ledgers can be of the same or different types (e.g. Ethereum, Hyperledger Fabric, Hyperledger Indy, or KSI), and once triggered, Interledger passes a customisable payload from the *Initiator* ledger to the *Responder* ledger(s).

One major limitation of the FIB design is centralization, meaning that the single node running the interledger bridge has to be trusted, with post hoc auditability only. This is enough for applications where a single party is involved. However, for multiple parties to cooperate, shared trust is required in interledger transactions, ensuring that no one is misbehaving and undermines the interest of others. This naturally leads to a decentralized architecture of the DIB component. DIB component provides the shared trust among a consortium of participating parties for interledger transactions conducted, meanwhile improving the robustness of the interledger data transfer via redundancy.

To achieve a reasonable decentralized architecture for interledger, it is critical to make the following assumptions, that:

- Different parties form a consortium that run an application that involves interledger transactions, any party does not necessarily trust another
- Endpoints, which typically are smart contracts on distributed ledgers, at both source and destination of a data transfer will implement the interfaces required by FIB
- Interledger nodes controlled by different parties in a consortium have the same full access to the endpoints, including both read and write operations
- Interledger bridges at any nodes are equal in a sense, that there is no special or admin bridge with superior functionality or access

The distributed applications utilising the Interledger component can utilise the payload functionality to implement any customised features. Examples of how Interledger can be utilised include:
- [Transfering Data](/doc/example-data_transfer.rst) from one ledger to another.
- [Storing Data Hashes](/doc/adapter-ksi.md) stores detailed information in a (private) ledger and a hash of the information is then  stored in a (public) ledger at suitable intervals using Interledger to benefit from the higher trust of a public ledger.
- [Game Asset Transfer](/doc/example-game_asset_transfer.rst) implements a state transfer protocol, which is used for managing in-game assets: the assets can either be used in a game or traded between gamers. For both activities, a separate ledger is used and Interledger ensures that each asset is active in only one of the ledgers.
- [Hash Time Locked Contracts (HTLCs)](/doc/example-HTLC.md) describes how to use the Interledger to automate the asset exchange between two ledgers using Hash Time-Locked Contracs (HTLCs).
- [Automated Responsible Disclosure of Security Vulnerabilities](https://ieeexplore.ieee.org/document/9606687) publication describes how the Interledger can be utilised in disclosing security vulnerabilities in a secure and transparent manner.

### Architecture Overview
The Interledger component can run one or more *Interledger instances* in parallel. Each instance provides a unidirectional transaction with clear roles: one ledger acts as the *Initiator* that triggers the transaction, and the other ledger acts as the *Responder* that reacts to the trigger. The *Initiator* can also send a data payload to the *Responder(s)*, but the *Responder(s)* can only reply a success/fail status, so the Interledger functions as a unidirectional data transfer from the *Initiator* to the *Responders*. However, a pair of ledgers can also be configured for bidirectional data transfer by defining two instances in opposite directions as described in the [Configuration](#Configuration) section.

#### Adaptive design

![Interledger](doc/figures/Interledger-usage.png)
*Figure 1: Interledger module usage with relevant interfaces*

On some ledgers (e.g. Ethereum and Hyperledger Fabric), the Interledger communicates with an application smart contract on the ledger (the smart contract has to implement the *Initiator* and/or the *Responder* [interface](/doc/Interledger_internals.rst#ledger-interfaces)), while on others (e.g. Hyperledger Indy and KSI), no smart contract is required (or even available) and the Interledger communicates with the ledger directly. The ability to act as the Initiator and/or the Responder is normally included in the smart contract implementing the application logic, but separate proxy contracts can also be used as wrappers to interface with the Interledger component as has been done e.g. in the [Food Supply Chain pilot](https://media.voog.com/0000/0042/0957/files/sofie-onepager-food_final.pdf) (see the pilot's [smart contracts](https://github.com/orgs/SOFIE-project/projects/1) for details). 

As shown in figure 1, the Interledger component is run on a server and for each configured *Interledger instance* it listens for events (*InterledgerEventSending*) from the *Initiator*, which triggers the Interledger to call the *interledgerReceive()* function on the *Responder*. Once the *Responder* is finishied processing the transaction, it emits either *InterledgerEventAccepted* event, which triggers the Interledger to call *interledgerCommit()* function of the *Initiator*, or the *InterledgerEventRejected* event, which triggers the Interledger to call the *interledgerAbort()* function of the *Initiator*. If multiple responders have been defined in an instance, either all-N or k-out-of-N of them have to succeed (depending on which configuration was chosen), otherwise the whole transaction is rolled back and the Interledger calls the *interledgerAbort()* function of the *Initiator*. Note: multiple responders are supported only for a single-node Interledger.

The *id* parameter of *InterledgerEventSending* is only for the *Initiator's* internal use: each transaction can have a unique *id* or multiple transactions can share the same *id* depending on the needs of the application smart contract. However, the *id* is not passed to the *Responder*, only the *data* parameter is (so if the *Initiator* wishes to send the *id* also to the Responder, it has to  be included in the *data* parameter). Internally, the Interledger component processes each transaction individually, so each transaction is given a unique *nonce*, which is used in all activities with the *Responder*. Finally, when the success/failure of the transaction is communicated back to the *Initiator*, the nonce is mapped back to the original *id*.

Internally, as shown in Figure 2, the Interledger component is composed of two types of elements:
- a single *core*  that manages the transactions between ledgers. It passes the transactions from the *Initiator adapter* to the correct *Responder adapter(s)* and the corresponding success/fail statuses back to the *Initiator adapter*, enforces the all-N and k-out-of-N rules for transactions with multiple responders, but does no processing on the data payload.
- *DLT adapters (Initiators and Responders)* links the core with the different ledger types. The adapter is responsible for translating between the core and the ledger's native commands (listening for events and calling functions). Normally, the adapter does no processing on the data payload, but it is possible to implement adapters with specific processing functionality, if the ledger itself lacks suitable functionality (e.g. the KSI Responder Adapter calculates a hash of the data payload and stores the hash to the KSI ledger).

<img width="75%" src="doc/figures/Interledger-overview-1.png">

*Figure 2: internal structure of the Interledger module*

Interledger component currently supports three DLT types: [Ethereum](/doc/adapter-eth.md), [Hyperledger Fabric](/doc/adapter-fabric.md), [Hyperledger Indy](/doc/adapter-indy.md) and [KSI](/doc/adapter-ksi.md). For Ethereum and Hyperledger Fabric, both an *Initiator adapter* and a *Responder adapter* are provided, while for Hyperledger Indy, only a *Initiator adapter* is provided, and for KSI only a *Responder adapter*. Other DLTs can be supported by [implementing adapters](/doc/Interledger_internals.rst#extending-interledger-to-support-additional-ledgers) for them. Support for additional ledgers is [Future Work](#Future-Work).

More details of the Interledger component's implementation and how it is utilised for *one-on-one* and *multi-ledger* transactions can be found in the [Technical description](/doc/Interledger_internals.rst).

#### Decentralized architecture

The single node FIB implementation consists of the Interledger Core, the Initiator and Responder adapters for multiple different ledgers. For simplicity, an Interledger Core, together with the adapters for a particular connection between endpoints can be defined as an interledger bridge instance which is symbolized as illustrated in Figure 3. Roughly the bridge logic can be divided into initiator logic that communicates with the source ledger and responder logic that communicates with the destination ledger.

![Instance](doc/figures/Bridge-instance.png)

*Figure 3: Symbol of bridge instance*

The high-level structure of the DIB design is illustrated in Figure 4 as below. The architecture consists of an Decentralized State Management (DSM) layer in the center for synchronizing the common understanding of interledger data transfer (or interledger transaction interchangeably), and interledger nodes that host interledger bridge instances. There is a set of addresses on the DSM that are owned by each interledger node and known by others that map to one (potential) bridge instance. In the illustration, endpoints, which is typically a smart contract on a distributed ledger, of each bridge are ignored for simplicity.

![Instance](doc/figures/DIB-architecture.png)

*Figure 4: Decentralized Interledger Bridge (DIB) architecture*

In this decentralized architecture, it should be guaranteed that the interledger nodes should always have access to the DSM layer that is shared among the consortium of partners. In other words, interledger nodes for DIB are the computers that share a consortium DSM and host bridges for connections of consortium interest.

For easy reference and formal definition, concepts and definitions of important modules in the DIB architecture are listed as below:

*Table 1: Modules in DIB architecture*
| Module/Concept   | Symbol | Type           | Description                                                                                                                                                                                                      |
|------------------|--------|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  Bridge instance |   Bi   |  Python module | An interledger bridge instance (or bridge) Bi is the software module that conducts communication between source and destination distributed ledgers of a connection Ci to fulfil a general-purpose data transfer |
| Interledger node |   Nx   |     Server     | An interledger node Nx is the server computer controlled by party x that have access to the DSM layer of a consortium and host bridge instances for connections                                                  |
|              DSM |   DSM  |     Ledger     | An Ethereum State Management (DSM) layer is an Ethereum consortium ledger that is set up to manage the states of interledger transactions of common interest by consortium partners                              |
|   Smart contract |   SCi  | Smart contract | A smart contract SCi is the code on DSM to manage and synchronize status of interledger transactions among bridges for a particular connection Ci           

*Table 2: Concepts relevant to DIB architecture*
| Module/Concept 	| Symbol 	| Type                                                                	| Description                                                                                                                                                                                                	|
|----------------	|--------	|---------------------------------------------------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
|     Connection 	|   Ci   	|                                 N/A                                 	| A connection is a communication configured between endpoints Es and Ed located at source and destination ledgers                                                                                           	|
|       Endpoint 	|  Es/Ed 	| Smart contract (account able to make ledger transaction in general) 	| An endpoint Es/Ed on either source or destination ledger is the application logic that initiates or receives the interledger data transfer, typically it is in the form of smart contract at these ledgers 	|
| Transfer entry 	|    t   	|                                 N/A                                 	| A transfer entry is an instance of interledger transaction at the DSM layer                                                                                                                                	|
| Smart contract 	|   SCi  	|                            Smart contract                           	| A smart contract SCi is the code on DSM to manage and synchronize status of interledger transactions among bridges for a particular connection Ci                                                          	|

### Key Technologies
The software modules are implemented in **Python**.
Currently the component supports the Ethereum ledger, and thus **Solidity** smart contracts, Hyperledger Fabric, as well as the KSI ledger.

***

## Usage

The `src/interledger` directory contains the code implementing the software modules of Interledger and the default adapters for Ethereum, Hyperledger Fabric and KSI.

The `ledgers/solidity/contracts` contains the smart contracts including the data transfer interfaces used by the component, while `dsm/contracts` contains the Connection smart contract used by the DSM layer.

### Prerequisites

Software modules: **Python 3.8**.

Smart contracts: **Solidity 0.7**.

Ganache CLI, Truffle, and Node.js 16 (older version of Node.js may work) to test Interledger locally.

### Installation

The dependencies of the Interledger component can be installed with the following commands (note that it is also possible to run the component using a docker image as described in the [Docker Images](#Docker-Images) section):

```bash
python3 -m pip install setuptools-rust
python3 -m pip install .
```

Python dependencies require Rust compiler and several libraries, check the [Dockerfile](docker/Dockerfile) for more details.

#### Install Smart Contracts using NPM

Users who only need the essential smart contracts related to Interledger data or asset transfer interfaces and their example implementations have the option to use the separate [SOFIE Interledger Contracts npm module](https://www.npmjs.com/package/sofie-interledger-contracts), without the need to include this whole repository.

```bash
npm install sofie-interledger-contracts
```

This command installs the module with all the essential smart contracts of interfaces and sample implementations. These can then be extended for a custom application logic.

### Configuration
The configuration file, following the `ini` format, has three main sections:

1) `[service]`: defines the connected ledgers, `left` and `right`, and the `direction` of the data transfer;
    - `direction` = `both` | `left-to-right` | `right-to-left`
    - `left` = *left*
    - `right` = *right*

2) `[left]`: indicates the `type` of that ledger and lists its options. The options depend on the specific ledger.  
    - `type` = `ethereum` | `fabric` | `indy` | `ksi` | ...
    - ...

3) `[right]`: same as above.
    - `type` = `ethereum` | `fabric` | `indy` |`ksi` | ...
    - ...

The `direction` can have three values:
- `left-to-right` means that a single unidirectional *Interledger instance* is started so that it listens for events on the `left` ledger with the *Initator adapter* and transfers data to the `right` ledger with the *Responder adapter*;
- `right-to-left` the same, but with inverse order;
- `both` means that the two *Interledger instances* will be started in opposite directions to allow transfering data in both directions and that both *Initiator* and *Responder adapters* will be instantiated for both ledgers.

`left` and `right` are custom names and provide all the options needed to setup the ledgers. The available options depend on the `type` of the ledger, and more details of [Ethereum](/doc/adapter-eth.md), [Hyperledger Fabric](/doc/adapter-fabric.md), [Hyperledger Indy](/doc/adapter-indy.md) and [KSI](/doc/adapter-ksi.md) configuration options are available in their respective documents. Finally, *left* and *right* can also be the same, so it is possible to use Interledger to connect smart contracts on the same ledger, which can be used e.g. in testing; in that case, section 3 can be omitted.

#### Configuration Example for Ethereum

For ledger `type` =  `ethereum`, the required options are:

- **url:** the ethereum network url (localhost or with [Infura](https://infura.io/));
- **port:** if the url is localhost;
- **minter:** the contract minter (creator) address, which is also the account triggering the operations on a ledger;
- **contract:** the contract address;
- **contract_abi:** path to the file describing the contract ABI in JSON format.

The optional options include:
- **poa:** needs to be set to True if Geth proof-of-authority consensus is used
- **ipc_path:** path to the IPC pipe of the ledger running locally, e.g.: /home/user/geth/geth.ipc, this overrides url/port settings

As an example, there is the Interledger configuration file *config-file-name.cfg* for Ethereum, which defines two ledgers that are running locally on ports 7545 and 7546:

    [service]
    direction=both
    left=left
    right=right

    [left]
    type=ethereum
    url=http://localhost
    port=7545
    minter=0x63f7e0a227bCCD4701aB459b837446Ce61aaEb6D
    contract=0x50dc31410Cae2527b034233338B85872BE67EEe6
    contract_abi=ledgers/contracts/GameToken.abi.json

    [right]
    type=ethereum
    url=http://localhost
    port=7546
    minter=0xc4C13639a867EfA9f863aF99A4c8d002E57198e0
    contract=0xba83df5f1DF4aB344240eC9F1E096790c88A216A
    contract_abi=ledgers/solidity/contracts/GameToken.abi.json

For public Ethereum networks, external providers such as [Infura](https://infura.io/) can be utilised to avoid running a full Ethereum node. For external providers the additional option is:

- **private_key** the private key of the minter account used to sign the transaction;

Specifically, when using the Infura endpoints, please use the websocket version only so that the events emitted can be listened for properly. An example can be found in the `[infura]` part of the sample configuration `configs/local-config.cfg`.

#### Configuration for DIB component

To run the DIB service, a DSM layer is required thus should be set up beforehand. The details of DSM layer setup can be found at the [DIB docs](doc/DIB.md) page.

To properly configure a DIB component, `dsm1` (and `dsm2` for bi-directional DIB setup) must be defined under the `[service]` section. Currently, only Ethereum is allowed to be used as the DSM layer. A concrete example is given in the `configs/dib.cfg` file, also shown below.

    [service]
    direction=both
    left=source_ledger
    right=dest_ledger
    dsm1=dsm_contract1
    dsm2=dsm_contract2

    [source_ledger]
    type=ethereum
    url=http://localhost
    port=7545
    minter=0x6393A03946b488fc9fBCED3dc6f71FD7a05CB510
    contract=0x4db13f4c6187216a2106828f971681fD61Ba8953
    contract_abi=ledgers/solidity/contracts/GameToken.abi.json

    [dest_ledger]
    type=ethereum
    url=http://localhost
    port=7546
    minter=0x7fb45E8916BF437Ca959197A53f40F673e342608
    contract=0x677f7079E3084152E5b8Fd7541e407AB15946975
    contract_abi=ledgers/solidity/contracts/GameToken.abi.json

    [dsm_contract1]
    type=ethereum
    url=http://localhost
    port=8545
    minter=0x7B07E042c3E3E4C48ADe280840E531214D48dEE1
    contract=0x766e70d73a83e86a4097409E9edB89dee640C862
    contract_abi=dsm/contracts/Connection.abi.json

    [dsm_contract2]
    type=ethereum
    url=http://localhost
    port=8545
    minter=0x7B07E042c3E3E4C48ADe280840E531214D48dEE1
    contract=0x766e70d73a83e86a4097409E9edB89dee640C862
    contract_abi=dsm/contracts/Connection.abi.json


In a case of bi-directional Interledger, two DSM entries (`dsm1` and `dsm2`) are needed, since a single DSM smart contract handles unidirectional Interledger connection. While in the example both DSM entries are using the same ledger instance, they could also be run on separate ledgers. If private key of the DSM layer is needed, same format applies following the `[infura]` part of the sample configuration `configs/local-config.cfg` as mentioned in the previous section.

The number of DIB nodes is specified during the deployment of smart contracts, e.g. following commands deploy both the GameToken and DSM smart contracts:
```bash
cd ledgers/solidity && npx truffle migrate --reset --f 10 --to 10 --network left --nodes 2 --config_file ../../configs/dib.cfg
cd ledgers/solidity && npx truffle migrate --reset --f 10 --to 10 --network right --nodes 2 --config_file ../../configs/dib.cfg
cd dsm && npx truffle migrate --reset --f 2 --to 2 --network dsm_ledger --nodes 2 --config_file ../configs/dib.cfg
```

Here the `nodes` parameter denotes the number of DIB nodes, which also affects the minimal number of nodes needed for endorsment (by default, endorsment requires the smallest majority of nodes, although this can be freely chosen). The `config_file` parameter denotes the base configuration file which is used by the deployment scripts, the scripts will create `config_file-node-1.cfg`, `config_file-node-2.cfg`, ..., etc. which then are used to launch the separate DIB instances.


It is also possible to use DIB in a single-node mode, in which case state manager is run locally and no DSM ledger is needed. In this case type of dsm ledger should be `local`, for example:
    [dsm_contract1]
    type=local
    
    [dsm_contract2]
    type=local


### Execution

For local testing, ensure that local ledger instances are running, and smart contracts are deployed to them (check [testing](#Testing) section for an example).

Then, run the following command:

```bash
python3 start_interledger.py config-file-name.cfg
```

where `config-file-name.cfg` is a configuration file for the setup of the interledger component, following the previously described `ini` format.

This script will create an *Interledger instance(s)* according to the configuration file and then call the `Interledger.run()` routine, which will listen to events coming from the connected ledger(s). The script can be interrupted with: `^C`.

```bash
python3 start_dib.py config-file-name.cfg
```

Similary, the above command will lauch the *DIB bridge instances* with a specific configuration file. Note, the DSM ledger must be running before starting the DIB instances unless the local state manager is used.

There are multiple examples of utilising the Interledger component:

- [CLI demo app](/demo/cli) can be used to directly interact with the Interledger component.
- A simple example for [data transfer](/doc/example-data_transfer.rst) between two ledgers (both Ethereum or Hyperledger Fabric examples are included).
- Interledger component supports [storing hashes](/doc/adapter-ksi.md) to the [Guardtime KSI](https://guardtime.com/technology) blockchain using [Catena DB](https://tryout-catena.guardtime.net/swagger/) service.
- The [game asset transfer](/doc/example-game_asset_transfer.rst) example shows how a protocol for enforcing that in-game assets are only active in one of the connected ledgers at a time can be built on top of the Interledger.
- [Hash Time Locked Contracts (HTLCs)](/doc/example-HTLC.md) example describes how to use the Interledger to automate the asset exchange between two ledgers using HTLCs.


### Docker Images

Execute the script `sh scripts/docker-build.sh` to build a Docker image for the single-node Interledger component. Configuration file can be provided to the image at runtime with the command `docker run -v /path/to/config.cfg:/var/interledger/configs/local-config.cfg interledger`.

`docker/Dockerfile` contains multiple build targets:
- **build**: only installs dependencies
- **run_demo**: runs Interledger command line demo
- **run_dib**: runs DIB Interledger component
- **run (default)**: runs Inteledger component

`docker/Dockerfile.migrations` contains targets for compiling Ethereum smart contracts:
- **compose_migrations**: installs dependencies and compiles client smart contracts
- **compose_dib_migrations**: installs depencies and compiles DSM smart contract


**Docker Compose**

Docker Compose setup allows an easy usage of the Interledger [CLI demo](/demo/cli/README.md) by running `sh scripts/compose_dib_start.sh`. Note that starting the whole setup will take some time, especially for the first time when all the necessary Docker images are built, therefore it is important to allow the startup script to shutdown gracefully.

The setup contains two Ganache CLI instances that act as local ledgers, the three DIB compoents, and the command line demo, see `docker-compose-dib.yaml` for more details.

If there are any updates to the DIB component, example smart contracts, `Dockerfile`, or `docker-compose-dib.yaml`, run `docker-compose -f docker-compose-dib.yaml build` command to rebuild the containers.


***

## Testing

The `tests/` directory contains the scripts to test the software modules of the component, including unit tests, integration tests, and system tests, while the `ledgers/solidity/test/` directory contains the tests for the smart contracts.

### Prerequisites for Testing

The easiest way to run the tests for the component is by using [Tox](https://tox.readthedocs.io/en/latest/), which will install all dependencies for testing and run all the tests. It is also possible to run the tests directly using pytest, which also allows running tests independently.

Install Tox:

```bash
pip install tox
```

Or install pytest and dependencies:

```bash
pip install pytest pytest-asyncio
```

Some of the tests assume that local Ethereum networks are running. Ganache CLI tool can be used for this:

```bash
npm install -g ganache-cli
```

To run component tests requiring local Ethereum networks, and to test example smart contracts, install Truffle:

```bash
cd ledgers/solidity/
npm install
```

To run the tests for DSM layer, similar steps apply: navigate to the `dsm` directory, and install the dependencies required.

```bash
cd dsm
npm install
```

#### Environment

- [Truffle](https://www.trufflesuite.com/) to test the smart contracts (it includes the [Mocha](https://mochajs.org/) framework);
- The [pytest](https://docs.pytest.org/en/latest/getting-started.html) testing framework;
- The [pytest asyncio](https://github.com/pytest-dev/pytest-asyncio) library to test async co-routines.

### Running the Tests

#### Single-node FIB

First, local test networks need to be set up:

```bash
ganache-cli -p 7545 -b 1
ganache-cli -p 7546 -b 1
```

Here the block time is set to be one second as simulation of mining.

Afterwards, deploy the smart contracts to the local test networks:

```bash
make migrate-left
make migrate-right
```

Then, to test the component run:

```bash
tox
```

Read the [README](/tests/README.md) for pytest tests and test structure.

Note that testing the KSI support requires valid credentials for the Catena service. The tests can be run manually after adding credentials to `configs/local-config.cfg`:

```bash
pytest tests/system/test_ksi_responder.py tests/system/test_interledger_ethereum_ksi.py
```

Note that testing timeout handling requires starting `ganache-cli` with `-b <blocking-time>` parameter:
```bash
ganache-cli -b 1 -p 7545
ganache-cli -b 1 -p 7546

pytest tests/system/test_timeout.py
```   

To test the smart contracts located in the `solidity` directory, shutdown `ganache-cli` instances (they will block the tests) and run the following (smart contracts are compiled automatically):

```bash
make test-contracts
```

#### DIB testing 

##### Testing DSM layer smart contracts

Navigate to the `dsm` directory, and install the dependencies required.

```bash
cd dsm
npm install
```

To test the Connection smart contract on DSM layer

```bash
npm run test
```

or

```bash
npx hardhat test
```

##### Testing DIB module

First, local test networks for the source and destination endpoints need to be set up:

```bash
ganache-cli -p 7545 -b 1
ganache-cli -p 7546 -b 1
```

Another ledger is also required to form the DSM layer for Connection smart contract to reside:

```bash
ganache-cli -p 8545 -b 1
```

Here the block time is set to be one second as simulation of mining.

Afterwards, deploy the smart contracts including the application smart contracts on endpoints and connection smart contract on DSM layer to the above local test networks:

```bash
make migrate-endpoints-dsm
```

or do it step by step

```bash
make migrate-left-dsm 
make migrate-right-dsm
make migrate-dsm-ledger
```

Then the Interledger nodes can be started as follows (default configuration has two nodes):
```bash
python3 start_dib.py configs/dib-node-1.cfg
python3 start_dib.py configs/dib-node-2.cfg
```

And the demo application can be run as:
```bash
python3 demo/cli/cli.py configs/dib-node-1.cfg
```


Also the validation test based on the Game Token use case can be run as: 

And then the test can be run:
```bash
python -m pytest tests/system/test_dib.py --config_file=configs/dib-node-1.cfg
```

For testing the DIB with a local state manager, first contracts need to be deployed with:
```bash
make migrate-local-dsm
```

And then the test can be run:
```bash
python -m pytest tests/system/test_dib_local_dsm.py --config_file=configs/dib-local-node-1.cfg
```


### Evaluating the Results

When using Tox and Truffle, test results in JUnit format are stored in the `tests` directory. 
Files `python_test_results.xml` and `smart_contracts_test_results.xml` contain results for 
the Python and smart contracts' tests respectively.

### Test for multi-ledgers transaction

Integration tests for multi-ledgers mode have been included as well, with each step of the processing validated. It is not part of the default tests above, so to have to be run separately in the virtual envrionment with the following command:

```
python -m pytest --capture=sys tests/integration/test_interledger_multi.py
```

***

## Generating Documentation
A documentation file including the information provided by this readme and the docs for different modules and functions (both Python and Solidity) can be generated by using the [Sphinx](http://www.sphinx-doc.org/en/master/) tool. This section provides the commands to generate documentation in HTML and PDF formats.

### Requirements
- Install dependencies for generating documentation:
```bash
pip install 'sphinx<3.0.0' m2r sphinxcontrib-httpdomain sphinxcontrib-soliditydomain sphinxcontrib-seqdiag
```

- Solidity:
To generate code documentation for Solidity, install [soliditydomain](https://pypi.org/project/sphinxcontrib-soliditydomain/).

- PDF:
To generate documentation in PDF format, the `latexmk` package is required to be installed. Please follow the [instructions](http://www.sphinx-doc.org/en/master/usage/builders/index.html#sphinx.builders.latex.LaTeXBuilder). **warning! Does not work if Solidity files are included. Exlude them from the documentation if you want to generate PDF documentation.**

### Generation 

- For HTML docs
```bash
make html
```

- For PDF docs via LaTeX
```bash
make latexpdf
```

In case a new sphinx documentation project is created:
- select **yes** when the `sphinx quickstart` command asks for `autodoc`;
- include the lines below in `doc/conf.py`:

```python
import sys
sys.path.insert(0, os.path.abspath('..'))

extensions = ['sphinx.ext.autodoc',
    'sphinxcontrib.soliditydomain',
    'sphinx.ext.coverage',
    ]

# autodoc lookup paths for solidity code
autodoc_lookup_path = '../ledgers/solidity/contracts' # or any other path to smart-contracts
```

***

## Open Issues

- DIB does not support having multiple ongoing DIB transfers with the same id. The id can be reused only after the transfer utilizing the same id was finalized. 

- DIB supports currently only Ethereum ledgers for initiator and only Ethereum or KSI ledgers for responder. DIB doesn't support verifying transactions of other ledgers.

- Sometimes installing Node.js dependencies fails with a timeout error, this affects both the local installation and Docker compose setup. This seems to be a bug in npm: https://github.com/npm/cli/issues/3078 .

## Future Work

Some of the planned future improvements to the Interledger component include:


- Notification mechanism for Initiator and Responder ledgers for failed DIB transfers: if the DIB transfer fails for some reason, the client ledgers should be notified of this.

- Improving tests for DIB, including a Docker compose based setup for tests.

- Automated deployment of DIB instances based on new DSM Connection smart contract


***

## Release Notes

## Contact Information

**Contact**: Dmitrij Lagutin dmitrij.lagutin@aalto.fi

**Contributors**: can be found in [authors](AUTHORS)



## License

This component is licensed under the Apache License 2.0.
