# Solidity
compile:
	cd ledgers/solidity && npx truffle compile

migrate:
	cd ledgers/solidity && npx truffle migrate --reset

migrate-both: migrate-left migrate-right

migrate-left:
	cd ledgers/solidity && npx truffle migrate --reset --f 6 --to 6 --network left

migrate-right:
	cd ledgers/solidity && npx truffle migrate --reset --f 6 --to 6 --network right

migrate-endpoints-dsm: migrate-left-dsm migrate-right-dsm migrate-dsm-ledger

migrate-left-dsm:
	cd ledgers/solidity && npx truffle migrate --reset --f 10 --to 10 --network left --nodes 2

migrate-right-dsm:
	cd ledgers/solidity && npx truffle migrate --reset --f 10 --to 10 --network right --nodes 2

migrate-dsm-ledger:
	cd dsm && npx truffle migrate --reset --f 2 --to 2 --network dsm_ledger --nodes 2

migrate-local-dsm: migrate-local-dsm-left migrate-local-dsm-right

migrate-local-dsm-left:
	cd ledgers/solidity && npx truffle migrate --reset --f 10 --to 10 --network left --config_file ../../configs/dib-local.cfg

migrate-local-dsm-right:
	cd ledgers/solidity && npx truffle migrate --reset --f 10 --to 10 --network right --config_file ../../configs/dib-local.cfg

test-contracts:
	cd ledgers/solidity && npx truffle test


# Module testing
test-interledger:
	PYTHONPATH=$$PWD/src pytest tests/test_interledger.py -s

test-ethereum-ledger:
	PYTHONPATH=$$PWD/src pytest tests/test_ethereum_ledger.py -s

test-integration:
	PYTHONPATH=$$PWD/src pytest tests/test_integration.py -s


test-db-manager:
	PYTHONPATH=$$PWD/src pytest tests/test_db_manager.py -s

test-state-initiator-responder:
	PYTHONPATH=$$PWD/src pytest tests/test_state_initiator_responder.py -s

# Documentation
html:
	cd doc && make html

latexpdf:
	cd doc && make latexpdf
