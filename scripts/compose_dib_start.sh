#!/bin/sh
# Runs Compose setup for Interledger demo
# and removes shared volumes afterwards
docker-compose -f docker-compose-dib.yaml run --rm interledger_demo
docker-compose -f docker-compose-dib.yaml down -v
