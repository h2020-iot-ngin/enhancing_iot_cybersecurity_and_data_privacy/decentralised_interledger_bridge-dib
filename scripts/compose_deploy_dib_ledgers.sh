#!/bin/sh
# Used inside Docker Compose environment
# deploys smart contracts and runs Interledger afterwards


# TODO: wait until both Ganache instances are running
# ....

# deploy client smart contracts, this will update configuration file
cd ledgers/solidity/
npx truffle migrate --reset --f 10 --to 10 --network left_compose --nodes 3 \
	--config_file ../../configs/dib-compose.cfg
npx truffle migrate --reset --f 10 --to 10 --network right_compose --nodes 3 \
	--config_file ../../configs/dib-compose.cfg
cd ../..

# deploy DIB smart contracts
cd dsm
npx truffle migrate --reset --f 2 --to 2 --network dsm_compose --nodes 3 \
	--config_file ../configs/dib-compose.cfg
cd ..

# copy the updated configuration files to shared directory
cp configs/dib-compose-node-1.cfg configs/dib-compose-node-2.cfg \
	configs/dib-compose-node-3.cfg config/
