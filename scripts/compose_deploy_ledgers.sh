#!/bin/sh
# Used inside Docker Compose environment
# deploys smart contracts and runs Interledger afterwards


# TODO: wait until both Ganache instances are running
# ....

# use different configuration file for Compose environment
cp configs/compose.cfg local-config.cfg

# deploy smart contracts, this will update configuration file
cd ledgers/solidity/
npx truffle migrate --reset --f 6 --to 6 --network left_compose
npx truffle migrate --reset --f 6 --to 6 --network right_compose
cd ../..

# copy the updated configuration file to shared directory
cp local-config.cfg config/
