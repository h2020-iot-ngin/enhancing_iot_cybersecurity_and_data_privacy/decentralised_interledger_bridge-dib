#!/bin/sh
# Waits until file ('config/local-config.cfg' or given as command line argument)
# exists, and run the Interledger demo afterwards
if [ $# -lt 1 ]
  then
	config_file="config/local-config.cfg"
  else
    config_file="$1"
fi

while [ ! -r "$config_file" ]
do
	sleep 1
done

python3 demo/cli/cli.py $config_file
