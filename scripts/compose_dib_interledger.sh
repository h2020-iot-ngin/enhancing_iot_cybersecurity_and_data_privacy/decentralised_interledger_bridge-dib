#!/bin/sh
# Waits until file ('config/local-config.cfg' or given as command line argument)
# exists, and runs the DIB Interledger afterwards
if [ $# -lt 1 ]
  then
	config_file="config/local-config.cfg"
  else
	config_file="$1"
fi

while [ ! -r "$config_file" ]
do
	sleep 1
done

python3 start_dib.py $config_file
