echo "before migrating connection..."
cat /var/config/dib.cfg

npx truffle migrate --reset --f 2 --to 2 --network dsm_ledger

echo "Migration for Connection done!"
cat /var/config/dib.cfg