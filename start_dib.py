import sys, asyncio
from web3 import Web3
from configparser import ConfigParser
import json

from src.interledger.dil import DecentralizedInterledger
from src.interledger.adapter.ethereum import EthereumInitiator, EthereumResponder, EthereumMultiResponder
from src.interledger.adapter.ksi import KSIResponder
from src.interledger.adapter.fabric import FabricInitiator, FabricResponder
from src.interledger.adapter.state_manager import EthereumILStateManager, LocalILStateManager
from src.interledger.utils import parse_ethereum, parse_ksi, parse_indy, parse_fabric

# Builder a left to right DIB instance
# Note: KSI is only supported as destination ledger
def left_to_right_bridge(parser, left, right, dsm):
    initiator = None
    responder = None
    state_manager = None
    ledger_left = parser.get(left, 'type')
    ledger_right = parser.get(right, 'type')
    ledger_dsm = parser.get(dsm, 'type')

    # Left ledger with initiator
    if ledger_left == "ethereum":
        cfg = parse_ethereum(parser, left)

        # Create Initiator
        initiator = EthereumInitiator(cfg)
    elif ledger_left == "fabric":
        (net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name) = parse_fabric(parser, left)
        # Create Initiator
        initiator = FabricInitiator(net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name)
    else:
        print(f"ERROR: ledger type {ledger_left} not supported yet")
        exit(1)
    
    # Right ledger with responder
    if ledger_right == "ethereum":
        cfg = parse_ethereum(parser, right)

        # Create Responder
        responder = EthereumResponder(cfg)
    elif ledger_right == "ksi":
        (url, hash_algorithm, username, password) = parse_ksi(parser, right)
        # Create Responder
        responder = KSIResponder(url, hash_algorithm, username, password)
    elif ledger_right == "fabric":
        (net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name) = parse_fabric(parser, left)
        # Create Responder
        responder = FabricResponder(net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name)
    else:
        print(f"ERROR: ledger type {ledger_right} not supported yet")
        exit(1)

    # State manager
    if ledger_dsm == "ethereum":
        cfg = parse_ethereum(parser, dsm)
        state_manager = EthereumILStateManager(cfg)
    elif ledger_dsm == "local": # local state manager
        state_manager = LocalILStateManager()
    else:
        print(f'ERROR: DSM ledger type not supported')
        exit(1)

    return (initiator, responder, state_manager)


# Builder a right to left DIB instance
# Note: KSI is only supported as destination ledger
def right_to_left_bridge(parser, left, right, dsm):
    initiator = None
    responder = None
    state_manager = None
    ledger_left = parser.get(left, 'type')
    ledger_right = parser.get(right, 'type')
    ledger_dsm = parser.get(dsm, 'type')
    
    # Right ledger with initiator
    if ledger_right == "ethereum":
        #(minter, contract_address, contract_abi, url, port, private_key, password, poa, ipc_path) = parse_ethereum(parser, right)
        cfg = parse_ethereum(parser, right)
        
        # Create Initiator
        initiator = EthereumInitiator(cfg)
    elif ledger_right == "fabric":
        (net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name) = parse_fabric(parser, left)
        # Create Initiator
        initiator = FabricInitiator(net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name)
    else :
        print(f"ERROR: ledger type {ledger_right} not supported yet")
        exit(1)

    # Left ledger with Responder
    if ledger_left == "ethereum":
        cfg = parse_ethereum(parser, left)

        # Create Responder
        responder = EthereumResponder(cfg)
    elif ledger_left == "ksi":
        (url, hash_algorithm, username, password) = parse_ksi(parser, left)
        responder = KSIResponder(url, hash_algorithm, username, password)
    elif ledger_left == "fabric":
        (net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name) = parse_fabric(parser, left)
        # Create Responder
        responder = FabricResponder(net_profile, channel_name, cc_name, cc_version, org_name, user_name, peer_name)
    else :
        print(f"ERROR: ledger type {ledger_left} not supported yet")
        exit(1)

    if ledger_dsm == "ethereum":
        cfg = parse_ethereum(parser, dsm)
        state_manager = EthereumILStateManager(cfg)
    elif ledger_dsm == "local": # local state manager
        state_manager = LocalILStateManager()
    else:
        print(f'ERROR: DSM ledger type not supported')
        exit(1)

    return (initiator, responder, state_manager)


def main():
    # Parse command line iput 
    if len(sys.argv) <= 1:
        print("ERROR: Provide a *.cfg config file to initialize the Decentralized Interledger instance.")
        exit(1)
    parser = ConfigParser()
    parser.read(sys.argv[1])

    # Get direction
    direction = parser.get('service', 'direction')
    left = parser.get('service', 'left')
    right = parser.get('service', 'right')
    dsm1 = parser.get('service', 'dsm1')
    
    # Build interledger bridge(s)
    dib_left_to_right = None
    dib_right_to_left = None

    if direction == "left-to-right":
        (initiator, responder, state_manager) = left_to_right_bridge(parser, left, right, dsm1)
        dib_left_to_right = DecentralizedInterledger(initiator, responder, state_manager)
    elif direction == "right-to-left":
        (initiator, responder, state_manager) = right_to_left_bridge(parser, left, right, dsm1)
        dib_right_to_left = DecentralizedInterledger(initiator, responder, state_manager)
    elif direction == "both": # dsm2 is ledger in other direction
        dsm2 = parser.get('service', 'dsm2')
        (initiator_lr, responder_lr, state_manager_lr) = left_to_right_bridge(parser, left, right, dsm1)
        (initiator_rl, responder_rl, state_manager_rl) = right_to_left_bridge(parser, left, right, dsm2)
        dib_left_to_right = DecentralizedInterledger(initiator_lr, responder_lr, state_manager_lr)
        dib_right_to_left = DecentralizedInterledger(initiator_rl, responder_rl, state_manager_rl)
    else:
        print("ERROR: supported 'direction' values are 'left-to-right', 'right-to-left' or 'both'")
        print("Check your configuration file")
        exit(1)

    task = None

    if dib_left_to_right and dib_right_to_left:
        future_left_to_right = asyncio.ensure_future(dib_left_to_right.run())
        future_right_to_left = asyncio.ensure_future(dib_right_to_left.run())
        task = asyncio.gather(future_left_to_right, future_right_to_left)
        print("Starting running routine for *double* sided interledger")
    elif dib_left_to_right:
        task = asyncio.ensure_future(dib_left_to_right.run())
        print("Starting running routine for *left to right* interledger")
    elif dib_right_to_left:
        task = asyncio.ensure_future(dib_right_to_left.run())
        print("Starting running routine for *right to left* interledger")
    else:
        print("ERROR while creating tasks for interledger")
        exit(1)

    return (task, dib_left_to_right, dib_right_to_left)

if __name__ == "__main__":
    (task, dib1, dib2) = main()
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(task)
    except KeyboardInterrupt as e:
        print("-- Interrupted by keyword --")
        if dib1: 
            dib1.stop()
        if dib2:
            dib2.stop()
        loop.run_until_complete(task)
        loop.close()
        print("-- Finished correctly --")
