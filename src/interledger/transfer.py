from enum import Enum, IntFlag, IntEnum


class TransferStatus(Enum):
    """State of a data transfer during the interledger protocol
    """
    INIT = 1
    INQUIRED = 2
    ANSWERED = 3
    SENT = 4
    RESPONDED = 5
    CONFIRMING = 6
    FINALIZED = 7


class Transfer(object):
    """The information paired to a data transfer: its 'future' async call to accept(); the 'result' of the accept();
    the transfer 'state'; the event transfer 'data'.
    """

    def __init__(self):
        self.status = TransferStatus.INIT
        self.payload = None  # transactional bundle, {id, nonce, data}
        # denote whether the send task has been accepted by an IL Node
        self.send_accepted = False
        self.send_task = None
        # denote whether the confirm task has been accepted by an IL Node
        self.confirm_accepted = False
        self.confirm_task = None

        # result of current ledger transaction
        self.result = {}


class TransferToMulti(Transfer):
    """The information of a data transfer, that is aimed to for multi-ledger targets
    """

    def __init__(self):
        super().__init__()
        self.inquiry_tasks = None
        self.inquiry_results = None
        self.inquiry_decision = None
        self.send_tasks = None
        self.results = None


class DecentralizedTransferStatus(IntEnum):
    """State of a data transfer in the decentralized interledger protocol
    """
    NotApplicable = 0, # before set up, flag for non-existing transfer
    Initialized = 1, # initial state when a data transfer is set up
    InitializedEndorsed = 2, # Initialized gets endorsed by required consortium members
    Sent = 3, # data is sent to destination
    Accepted = 4, # data transfer is accepted by destination
    AcceptedEndorsed = 5, # state Accepted gets endorsed by required consortium members
    Rejected = 6, # data transfer is rejected by destination
    RejectedEndorsed = 7, # state Rejected gets endorsed by required consortium members
    Confirming = 8, # response above is fed back to source side
    Committed = 9, # data transfer is committed
    CommittedEndorsed = 10, # state Committed gets endorsed by required consortium members
    Aborted = 11, # data transfer is aborted
    AbortedEndorsed = 12, # state Aborted gets endorsed by required consortium members
    Declined = 13 # invalid action or misbehaving is detected

class DecentralizedEventType(IntFlag):
    """Type of event coming from DSM: entry created or entry updated
    """
    EntryCreated = 1,
    EntryUpdated = 2


class DecentralizedTransfer(Transfer):
    """Similar to the Transfer object, the information paired to a data transfer, in a decentralized interledger protocol
    """

    def __init__(self,
                 status=DecentralizedTransferStatus.Initialized,
                 transfer: Transfer=None):
        self.status: DecentralizedTransferStatus = status

        # transactional bundle, {id, nonce, data}
        self.payload = transfer.payload if transfer else {}
        
        # metadata about the transfer: {blockNumber, transactionHash, additionalData, nonce}
        self.metadata = transfer.metadata if transfer else {}

        # denote whether the send task has been accepted by an IL Node
        self.send_accepted = transfer.send_accepted if transfer else False
        self.send_task = transfer.send_task if transfer else None
        # denote whether the confirm task has been accepted by an IL Node
        self.confirm_accepted = transfer.confirm_accepted if transfer else False
        self.confirm_task = transfer.confirm_task if transfer else None

        # denote the endorsement and decline status
        self.endorsement = 0
        self.is_endorsed = False
        self.is_declined = False

        # result of current ledger transaction
        self.result = transfer.result if transfer else {}
        
        # timestamp of the last action (used for checking timeouts)
        self.timestamp = 0
        
