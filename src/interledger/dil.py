from asyncio.tasks import create_task
from typing import AnyStr, Dict, Union, List
from uuid import uuid4
import asyncio
import functools

from datetime import datetime
from pprint import pprint

from .adapter.interfaces import Initiator, Responder, ILStateManager, LedgerType, ErrorCode
from .interledger import Interledger
from .transfer import DecentralizedTransfer, DecentralizedTransferStatus, Transfer, TransferStatus
from .transfer import DecentralizedEventType

class DecentralizedInterledger(Interledger):
    """
    Class definition of decentralized extended version of Interledger instance
    """

    def __init__(self, initiator: Initiator,
                 responder: Union[Responder, List],
                 state_manager: ILStateManager,
                 multi: bool = False,
                 threshold: int = 1):

        super().__init__(initiator, responder, False, threshold) # multi-ledger is disabled for v2.0.x
        
        # # transfers monitored by super
        self.transfers: Dict[str, Transfer] = {}
        self.transfers_sent = []
        # self.transfers_responded = []
        self.results_committing = []
        self.results_aborting = []
        self.results_commit = []
        self.results_abort = []

        self.state_manager = state_manager
        
        self.fail_sending = 0

    async def run(self):
        print("*******************************************")
        print("DIL running...")
        print("*******************************************")
        self.up = True

        # for testing timeouts
        #now = datetime.now()
        #if (self.initiator.minter == "0x7eAB55777cCc8cd2CFF0f7DE7BBdE5b9fCde1520") ^ (now.minute % 2):
            #print("timeout testing node")
            #self.fail_sending = 1
            
            
        while self.up:
            # Triggers
            
            receive = asyncio.ensure_future(self.receive_transfer())
            listen = asyncio.ensure_future(self.state_manager.wait_for_events())
            
            if not self.transfers_sent \
                and not self.results_committing \
                and not self.results_aborting:
                    await asyncio.wait([receive, listen], return_when=asyncio.FIRST_COMPLETED)
            #await receive
            #await listen
            
            result = asyncio.ensure_future(self.transfer_result())
            confirm = asyncio.ensure_future(self.confirm_transfer())
            #await asyncio.wait([check, result, confirm], return_when=asyncio.FIRST_COMPLETED)
            await asyncio.wait([result, confirm], return_when=asyncio.FIRST_COMPLETED)
            
            # Actions
            # create new DSM entries based on self.transfers
            create = asyncio.ensure_future(self.create_entry())
            send = asyncio.ensure_future(self.send_transfer())

            # validate transfers that have been recorded in self.state_manager.transfers_to_validate
            validate = asyncio.ensure_future(self.validate_transfers())
            
            process = asyncio.ensure_future(self.process_result())
            await create
            await send            
            await validate
            await process
            #await asyncio.wait([result, confirm, create, validate, send, process], return_when=asyncio.FIRST_COMPLETED)
            #await asyncio.wait([send, process], return_when=asyncio.FIRST_COMPLETED)
            
            self.check_timeouts()

            # clean up
            self.cleanup()
            #await asyncio.sleep(0.1)

        # TODO Add some closing procedure

        
    async def validate_transfers(self):
        """Validates all transfers in self.state_manager.transfers_to_validate
        """
        
        for transfer_id in list(self.state_manager.transfers_to_validate):
            #print("\t\tvalidate_transfer: ", transfer_id)
            transfer = self.state_manager.transfers_to_validate[transfer_id]
            response = await self.validate_dsm_transfer(transfer)
            if (response):
                del self.state_manager.transfers_to_validate[transfer_id]
                
        #print("\tvalidate_transfer, list is now:", self.state_manager.transfers_to_validate.items())

        

    async def validate_dsm_transfer(self, transfer: DecentralizedTransfer) -> bool:
        """Validates that the transfer entry on DSM ledger ledger is correct
           calls endorse_action if successful, decline_action otherwise
           returns True or False
        """
        
        event_match = 0 # wherever we found corresponding event
        call_match = 1  # wherever we found corresponding function call
        
        now = datetime.now()
        print(f"{now}: Validating transfer {transfer.payload['id']}, state: {transfer.status}")


        # check that final transaction has been processed correctly on Initiator's side
        # for this step there is no need to check for events
        if (transfer.status == DecentralizedTransferStatus.Committed) or \
            (transfer.status == DecentralizedTransferStatus.Aborted):

            #print("dil.py: Validate Committed/Aborted:", transfer.metadata)

            # get transaction details
            tx = self.initiator.web3.eth.get_transaction(transfer.metadata['transactionHash'])
            tx_function, tx_parameters = self.initiator.contract.decode_function_input(tx["input"])

            # get the object of function that should have been called
            if transfer.status == DecentralizedTransferStatus.Committed:
                # check wherever we are using KSI ledger
                # FIXME: KSI return hash isn't stored on DSM!
                if self.responder.ledger_type == LedgerType.KSI:
                    function = self.responder. \
                        contract.get_function_by_signature('interledgerCommit(uint256, bytes)')
                else:
                    function = self.responder. \
                        contract.get_function_by_signature('interledgerCommit(uint256)')
            else: # Aborted
                function = self.responder. \
                    contract.get_function_by_signature('interledgerAbort(uint256,uint256)')
                call_match = 0
                # check also that the reason is correct
                if tx_parameters['reason'] == ErrorCode.TRANSACTION_FAILURE:
                    call_match = 1

            # verify that blockNumber, function, and id match
            if (tx['blockNumber'] == transfer.metadata['blockNumber']) and \
                (str(tx_function) == str(function)) and \
                (str(tx_parameters['id']) == str(transfer.payload['id'])):
                print("\tValidate Committed/Rejected: TX match")
                call_match = call_match + 1
                
            if call_match == 2: # endorse transfer
                response = await self.state_manager.endorse_action(transfer.payload['id'], transfer.status)
            else: # decline transfer
                response = await self.state_manager.decline_action(transfer.payload['id'], transfer.status)

            return True

       
        # check interledgerReceive() call for Accepted/Rejected
        if (transfer.status == DecentralizedTransferStatus.Accepted) \
            or (transfer.status == DecentralizedTransferStatus.Rejected):
                
            call_match = 0
            
            # get transaction details
            tx = self.responder.web3.eth.get_transaction(transfer.metadata['transactionHash'])
            tx_function, tx_parameters = self.responder.contract.decode_function_input(tx["input"])
            
            # get the object of function that should have been called
            receive_function = self.responder. \
                contract.get_function_by_signature('interledgerReceive(uint256,bytes)')
            
            # verify that blockNumber, function, nonce, and data match
            if (tx['blockNumber'] == transfer.metadata['blockNumber']) and \
                (str(tx_function) == str(receive_function)) and \
                (tx_parameters['nonce'] == transfer.metadata['nonce']) and \
                (tx_parameters['data'] == transfer.payload['data']):
                #print("\tValidate Accepted/Rejected: TX match")
                call_match = 1
        
        # check the status of entry => this affects wherever we need to check data
        # on initiator or responder ledger
        if transfer.status == DecentralizedTransferStatus.Initialized:
            # check wherever the initial IL transaction is correct on the initiator side
            #print("dil.py: Validate Initial:", transfer.metadata)
            filt = self.initiator.contract.events.InterledgerEventSending() \
                .createFilter(fromBlock = transfer.metadata['blockNumber'], 
                              toBlock = transfer.metadata['blockNumber'])
            
        if transfer.status == DecentralizedTransferStatus.Accepted:
            # check transaction on the responder side
            #print("dil.py: Validate Accepted:", transfer.metadata)
            filt = self.responder.contract.events.InterledgerEventAccepted() \
                .createFilter(fromBlock = transfer.metadata['blockNumber'], 
                              toBlock = transfer.metadata['blockNumber'])

        if transfer.status == DecentralizedTransferStatus.Rejected:
            # check transaction on the responder side
            #print("dil.py: Validate Rejected:", transfer.metadata)
            filt = self.responder.contract.events.InterledgerEventRejected() \
                .createFilter(fromBlock = transfer.metadata['blockNumber'], 
                              toBlock = transfer.metadata['blockNumber'])
            
        if not filt:
            print(f"{now}:\tERROR: Filter doesn't exist")
            return
        entries = filt.get_all_entries()
          
        # if initialized => check id and data, if accepted/rejected => check nonce
        for entry in entries:
            # check that transactionHash and logIndex match
            # FIXME: currently logIndex isn't checked since it causes problem with multiple transfers...
            # need to strip 0x from the tx_hash in the event
            #if (transfer.metadata['transactionHash'] == entry['transactionHash'].hex()[2:]) \
                #and (transfer.metadata['additionalData'] == entry['logIndex']):
            if transfer.metadata['transactionHash'] == entry['transactionHash'].hex()[2:]:
                # check that id and data match
                if transfer.status == DecentralizedTransferStatus.Initialized:
                    if (transfer.payload['id'] == str(entry['args']['id'])) \
                        and (transfer.payload['data'] == entry['args']['data']):
                        #print("\tValidate/Initialized: found valid entry for:", transfer.payload['id'])
                        event_match = 1
                        
                if (transfer.status == DecentralizedTransferStatus.Accepted) or \
                    (transfer.status == DecentralizedTransferStatus.Rejected):

                    if transfer.metadata['nonce'] == entry['args']['nonce']:
                        #print("\tValidate/Accepted/Rejected: found valid entry for:", transfer.payload['id'])
                        event_match = 1


            
        if event_match and call_match: # endorse transfer
            response = await self.state_manager.endorse_action(transfer.payload['id'], transfer.status)
        else: # decline transfer
            response = await self.state_manager.decline_action(transfer.payload['id'], transfer.status)
            
        return True


    # Trigger
    async def receive_transfer(self):
        """Receive the list of transfers from the Initiator.
        """
        # print(f'before receive transfers, {self.transfers}')
        # retrieve Transfer objects
        transfers_raw = await self.initiator.listen_for_events()

        #if transfers_raw:
            #print("\n--- receive transfer actully working ... ---")
            #print(transfers_raw)
        
        for transfer in transfers_raw:
            id = transfer.payload['id']
            # record in core for check & endorsement logic
            self.transfers[id] = transfer
            now = datetime.now()
            #print(f'{now}: New transfer received with id: {id}, creating new entry to DSM')
            #print("\treceive_transfer: ", self.transfers[id])
        # print(f'after receive transfer, {self.transfers}')
        return len(transfers_raw)

    # Action
    async def create_entry(self):
        """Create entry for received transfers at the DSM layer
        """
        tasks = []
        for transfer in self.transfers.values():
            if transfer.status != TransferStatus.INIT:
                continue
            
            #print("dil.py: create_entry()")
            # convert to DecentralizedTransfer before create entry on DSM
            dt = DecentralizedTransfer(
                DecentralizedTransferStatus.Initialized, \
                transfer)
            
            transfer.status = TransferStatus.INQUIRED
            
            tasks.append(asyncio.create_task(self.state_manager.create_entry(dt.payload['id'], dt)))

        for task in tasks:
            await task
        
        return

    # Action
    async def validate_entry(self):
        """Validate entry changes on DSM and endorse / decline accordingly
        """
        # print(f'before validate entry, {self.transfers}')
        
        # print("validate entry working...")
        # print(self.state_manager.transfers_created)
        # check validity of transfers created
        for dt in self.state_manager.transfers_created.values():
            print("---validating transfer created...---")
            id = dt.payload['id']
            t_check = next(
                (t for t in self.transfers.values() \
                    if t.payload['id'] == id), \
                None)

            if not t_check:
                continue # TODO: timeout logic if no match found

            id = dt.payload['id']
            data = dt.payload['data']
            
            if id == t_check.payload['id'] and data == t_check.payload['data']:
                print("transfer created validated, endorsing...")
                res = await self.state_manager.endorse_action(
                    id, 
                    DecentralizedTransferStatus.Initialized)
                print(f'endorsed entry initialized for {id}: {res}')
            else:
                await self.state_manager.decline_action(
                    id,
                    DecentralizedTransferStatus.Initialized)
        self.state_manager.transfers_created = {} # clean up
        # print(f'after validate entry, {self.transfers}')

        # check validity of transfers responded
        for dt in self.state_manager.transfers_responded.values():
            print("---validating transfer responded...---")
            id = dt.payload['id']
            t_check = next(
                (t for t in self.transfers.values() \
                    if t.payload['id'] == id), \
                None)
            if not t_check:
                continue # TODO: timeout logic if no match found

            id = dt.payload['id']
            data = dt.payload['data']
            
            if id == t_check.payload['id'] and data == t_check.payload['data']:
                print("transfer response validated, endorsing...")
                res = await self.state_manager.endorse_action(
                    id, 
                    dt.status) # Accepted / Rejected
                print(f'endorsed entry responded for {id}: {res}')
            else:
                await self.state_manager.decline_action(
                    id,
                    dt.status) # Accepted / Rejected
        self.state_manager.transfers_responded = {} # clean up

    # Action
    async def send_transfer(self):
        #print("send transfer running...")
        for dt in self.state_manager.transfers_to_send.values():
            #print(f"\tSending transfer for id: {dt.payload['id']}")
            payload = dt.payload
            id, data = payload['id'], payload['data'] # data optionally contains id in itself

            payload = self.transfers[id].payload
            if 'nonce' not in payload:
                payload['nonce'] = str(uuid4().int)
            nonce = self.transfers[id].payload['nonce']

            # signal accept to send the transfer to destination
            response = False
            try:
                #print(f'\tsignal accept send for id: {id}')
                response = await self.state_manager.signal_send_acceptance(id)
                #print(f"\tsignal result: {response}")
            except KeyError:
                continue # drop if transfer not exist on DSM layer, 

            dt.status = DecentralizedTransferStatus.Sent # to be consistent with DSM layer
            
            if not response: # another node approved transfer first
                continue 

            # convert back to Transfer
            t = self.transfers[id]
            t.status, t.payload = TransferStatus.SENT, dt.payload

            # send data & cache at DCore
            if self.fail_sending:
                print("failing send_data on purpose")
                continue
            t.send_task = asyncio.ensure_future(
                self.responder.send_data(nonce, data))
                
            self.transfers_sent.append(t)
            #print(f'transfers sent: {self.transfers_sent}')

        self.state_manager.transfers_to_send = { \
            id: t for id, t in self.state_manager.transfers_to_send.items() \
            if t.status == DecentralizedTransferStatus.InitializedEndorsed}
        # print(f'after send transfer, {self.transfers}')

    # Trigger (& Action for DIL)
    async def transfer_result(self):
        send_tasks = [transfer.send_task for transfer in self.transfers_sent]
        if not send_tasks:
            return

        await asyncio.wait(send_tasks, return_when=asyncio.FIRST_COMPLETED)
        for transfer in self.transfers_sent:
            if transfer.status == TransferStatus.SENT and transfer.send_task.done():
                id = transfer.payload['id']
                #print("--- transfer result... ---")

                # update status and result
                transfer.result = transfer.send_task.result()
                transfer.status = TransferStatus.RESPONDED
                #print(f'transfer id: {id}, status: {transfer.status}, result: {transfer.result}')

                # convert to decentralized one for DSM layer
                dt = DecentralizedTransfer(
                    DecentralizedTransferStatus.Sent, # before determined as below, set as Sent
                    transfer)
                dt.status = \
                    DecentralizedTransferStatus.Accepted if transfer.result['status'] else \
                    DecentralizedTransferStatus.Rejected
                
                dt.metadata['blockNumber'] = transfer.result['blockNumber']
                dt.metadata['transactionHash'] = transfer.result['tx_hash'].hex()
                dt.metadata['additionalData'] = transfer.result['additionalData']
                dt.metadata['nonce'] = transfer.result['nonce']
                #print("transfer_result: metadata is", dt.metadata)

                # update DSM layer in transfers responded in state manager
                try:
                    await self.state_manager.update_entry(id, dt.status, dt)
                except KeyError:
                    continue # drop if transfer not exist on DSM layer

        # # update records
        self.transfers_sent = [
            transfer for transfer in self.transfers_sent if transfer.status == TransferStatus.SENT]
        # print(f'after transfer result, {self.transfers}')

    # Action
    async def process_result(self):
        for dt in self.state_manager.transfers_to_finalize.values():
            #print("---processing result...---")
            id = dt.payload['id']

            #print("self.transfers: ", self.transfers[id])
            #print("process_result, dt,", pprint(vars(dt)))

            # signal accept to confirm the result
            res = False
            try:
                res = await self.state_manager.signal_confirm_acceptance(id)
            except KeyError:
                continue # drop if transfer not exist on DSM layer, TODO: timeout logic
            
	    # sometimes other nodes already process the transaction, and therefore 
	    # state manager removes transaction from its list
            if id in self.state_manager.transactions:
                if self.state_manager.transactions[id]\
                    == DecentralizedTransferStatus.Accepted:
                        dt.status = DecentralizedTransferStatus.Committed
                else: # abort the transfer to initator
                    dt.status = DecentralizedTransferStatus.Aborted
                
                if not res: # another node confirmed first
                    continue
            
                # convert back to Transfer
                t = self.transfers[id]
                t.status, t.payload = TransferStatus.CONFIRMING, dt.payload
            
                if self.fail_sending:
                    print("failing process_result on purpose")
                    continue

                if self.state_manager.transactions[id]\
                    == DecentralizedTransferStatus.Accepted:

                    # commit the transfer to initiator
                
                    # If the Responder ledger is KSI, pass the KSI id (stored in
                    # tx_hash field of transfer) to the Initiator's commit function
                    if self.responder.ledger_type == LedgerType.KSI:
                        t.confirm_task = asyncio.ensure_future(
                            self.initiator.commit_sending(id, transfer.result['tx_hash'].encode()))
                    else:
                        t.confirm_task = asyncio.ensure_future(
                            self.initiator.commit_sending(id))
                    self.results_committing.append(t)
                else:  # abort the transfer to initator
                    reason = ErrorCode.TRANSACTION_FAILURE
                    t.confirm_task = asyncio.ensure_future(
                        self.initiator.abort_sending(id, reason))
                    self.results_aborting.append(t)
                

        # update records
        #self.state_manager.transfers_to_finalize = [transfer for transfer in self.transfers_responded \
        #    if transfer.status == TransferStatus.RESPONDED]
        self.state_manager.transfers_to_finalize = { \
            id: t for id, t in self.state_manager.transfers_to_finalize.items() \
            if (t.status == DecentralizedTransferStatus.AcceptedEndorsed) or \
                (t.status == DecentralizedTransferStatus.RejectedEndorsed)}

        # print(f'after process result, {self.transfers}')

    async def confirm_transfer(self):
        confirm_tasks = [
            transfer.confirm_task for transfer in self.results_committing + self.results_aborting]
        if not confirm_tasks:
            return
        await asyncio.wait(confirm_tasks, return_when=asyncio.FIRST_COMPLETED)

        for transfer in self.results_committing:
            #print("---confirm transfer running ...---")
            if transfer.status == TransferStatus.CONFIRMING and transfer.confirm_task.done():
                id = transfer.payload['id']
                transfer.result = transfer.confirm_task.result()
                #print("commit transfer result", transfer.result)

                transfer.metadata['blockNumber'] = transfer.result['blockNumber']
                transfer.metadata['transactionHash'] = transfer.result['commit_tx_hash'].hex()

                if transfer.result['commit_status']:
                    await self.state_manager.update_entry(id, 
                        DecentralizedTransferStatus.Committed, transfer)
                else:
                    await self.state_manager.update_entry(id, 
                        DecentralizedTransferStatus.Declined)

                self.results_commit.append(transfer)
                transfer.status = TransferStatus.FINALIZED

        self.results_committing = [
            transfer for transfer in self.results_committing if transfer.status == TransferStatus.CONFIRMING]

        for transfer in self.results_aborting:
            if transfer.status == TransferStatus.CONFIRMING and transfer.confirm_task.done():
                id = transfer.payload['id']
                transfer.result = transfer.confirm_task.result()
                
                transfer.metadata['blockNumber'] = transfer.result['blockNumber']
                transfer.metadata['transactionHash'] = transfer.result['abort_tx_hash'].hex()

                if transfer.result['abort_status']:
                    # TODO: add endorse logic when adapter interfaces refactored
                    await self.state_manager.update_entry(id,
                        DecentralizedTransferStatus.Aborted, transfer)
                else:
                    await self.state_manager.update_entry(id,
                        DecentralizedTransferStatus.Declined)
                
                self.results_abort.append(transfer)
                transfer.status = TransferStatus.FINALIZED

        self.results_aborting = [
            transfer for transfer in self.results_aborting if transfer.status == TransferStatus.CONFIRMING]

    def check_timeouts(self):
        # check wherever some transfers are still in Sent/Confirming state
        # after the timeout has passed, if yes try to send/confirm them
        now = datetime.now()
        
        if self.state_manager.source_timeout > 0:
            for t in self.state_manager.transfers_confirming:
                if self.fail_sending:
                    continue
                if now.timestamp() > \
                   (self.state_manager.transfers_confirming[t].timestamp.timestamp() \
                    + self.state_manager.source_timeout):
                    #print("\tSource timeout reached for: ", t, "resending")
                    self.state_manager.transfers_to_finalize[t] = \
                        self.state_manager.transfers_confirming[t]
        
        if self.state_manager.destination_timeout > 0:
            for t in self.state_manager.transfers_sent:
                if self.fail_sending:
                    continue
                #print("Timeout handling: transfers_sent:", t, 
                #      self.state_manager.transfers_sent[t].timestamp)
                if now.timestamp() > \
                    (self.state_manager.transfers_sent[t].timestamp.timestamp() \
                    + self.state_manager.destination_timeout):
                    #print("\tDestination timeout reached for: ", t, "resending")
                    self.state_manager.transfers_to_send[t] = \
                        self.state_manager.transfers_sent[t]

        

    def cleanup(self):
        self.transfers = {id: transfer for id, transfer in self.transfers.items() \
            if transfer.status != TransferStatus.FINALIZED}

    def stop(self):
        """Stop the interledger run() operation
        """
        print("*******************************************")
        print("DIL stopped...")
        print("*******************************************")
        self.up = False

        self.transfers = {}

        self.transfers_sent = []
        #self.transfers_responded = []

        self.results_committing = []
        self.results_commit = []

        self.results_aborting = []
        self.results_abort = []
