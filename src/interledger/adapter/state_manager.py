import asyncio
import functools

from copy import deepcopy
import web3
from urllib.parse import urlparse
from typing import Dict, List
from threading import Thread
import time
from datetime import datetime

from .ethereum import Web3Initializer
from .interfaces import ILStateManager, ErrorCode
from ..transfer import DecentralizedTransferStatus, DecentralizedTransfer, DecentralizedEventType
from ..utils import DIBEthereumConfig


Web3 = web3.Web3

class LocalILStateManager(ILStateManager):
    def __init__(self, required: int=1) -> None:
        self.required = required
        
        # no timeouts for local state manager
        self.source_timeout = 0
        self.destination_timeout = 0
        
        # this is to be used in compatible single node mode and for DIL verification purpose
        self.local_transfers: Dict[str, DecentralizedTransfer] = {}  # local state layer in memory

        # transfers that need to be validated by DIL
        self.transfers_to_validate: Dict[str, DecentralizedTransfer] = {}

        # transfers to be sent to responder
        self.transfers_to_send: Dict[str, DecentralizedTransfer] = {}
        
        # transfers to be finalized (committed/abordted on initiator side)
        self.transfers_to_finalize: Dict[str, DecentralizedTransfer] = {}
        
        # transfers that has been accepted/rejected by the initiator
        self.transfers_responded: Dict[str, DecentralizedTransfer] = {}

        self.transfers_created: Dict[str, DecentralizedTransfer]= {} 
        self.transfers_ready: Dict[str, DecentralizedTransfer] = {}

        self.transactions: Dict[str, DecentralizedTransfer] = {}

        # only for local state manager
        self.events = []

    # to be compatible with Ethereum SM
    async def wait_for_events(self):
        return True

    async def create_entry(self,
                           id: str,
                           transfer: DecentralizedTransfer) -> bool:
        if id in self.local_transfers:
            return False

        # create entry at DSM layer
        self.local_transfers[id] = deepcopy(transfer)
        self.events.append(('EntryCreated', id, transfer.payload['data']))
        
        # since we are running a single-node version, store transfer directly to
        # transfers_to_send
        self.transfers_to_send[id] = transfer
        
        return True

    async def signal_send_acceptance(self,
                                     id: str) -> bool:
        if id not in self.local_transfers:
            raise KeyError("Transfer not exist")
        t: DecentralizedTransfer = self.local_transfers[id]

        # record signal at DSM layer
        if t.send_accepted:
            return False  # rejected

        t.status = DecentralizedTransferStatus.Sent
        t.send_accepted = True 
        # send task should not be recorded to DSM layer
        return True

    async def signal_confirm_acceptance(self, 
                                        id: str) -> bool:
        if id not in self.local_transfers:
            raise KeyError("Transfer not exist")
        t: DecentralizedTransfer = self.local_transfers[id]

        if t.confirm_accepted:
            return False # rejected

        t.status = DecentralizedTransferStatus.Confirming
        t.confirm_accepted = True 
        # confirm task should not be recorded to DSM layer
        return True

    async def update_entry(self,
                           id: str,
                           status: DecentralizedTransferStatus,
                           transfer: DecentralizedTransfer = None) -> bool:
        if id not in self.local_transfers:
            raise KeyError("Transfer not exist")
        t: DecentralizedTransfer = self.local_transfers[id]

        self.transactions[id] = status 

        # record signal at DSM layer
        t.status = status
        if transfer: # update transfer content if provided
            t.send_accepted = transfer.send_accepted
            t.confirm_accepted = transfer.confirm_accepted
        t.is_endorsed = False
        t.endorsement = 0
        self.events.append(('EntryUpdated', id, status))
        
        #print("local SM, update_entry: ", id, status, transfer)
        
        # Accepted/Rejected => transfer can be finalized on Initiator side
        # also change status to *Endorsed since DIB is expecting that
        if (status == DecentralizedTransferStatus.Accepted):
            transfer.status = DecentralizedTransferStatus.AcceptedEndorsed
            self.transfers_to_finalize[id] = transfer
        elif (status == DecentralizedTransferStatus.Rejected):
            transfer.status = DecentralizedTransferStatus.RejectedEndorsed
            self.transfers_to_finalize[id] = transfer

        if (transfer.status == DecentralizedTransferStatus.CommittedEndorsed) or \
            (transfer.status == DecentralizedTransferStatus.AbortedEndorsed):
                # remove transaction from self.transactions since it has been finalized
                self.transactions.pop(id, None)
        
        return True

    async def endorse_action(self, 
                             id: str, 
                             status: DecentralizedTransferStatus) -> bool:
        if id not in self.local_transfers:
            raise KeyError("Transfer not exist")
        t: DecentralizedTransfer = self.local_transfers[id]

        if status == DecentralizedTransferStatus.Accepted:
            print('pre endorsement status:')
            print(t.is_endorsed)
            print(t.endorsement)
            print(t.is_declined)

        print("endorse_action called")

        if t.is_declined:
            return False # declined already
        if t.is_endorsed:
            return True # endorsed already

        t.endorsement += 1
        if t.endorsement >= self.required:
            t.is_endorsed = True
            if status == DecentralizedTransferStatus.Initialized:
                t.status = DecentralizedTransferStatus.InitializedEndorsed
                self.events.append(("TransferReady", t.payload['id']))
            elif status == DecentralizedTransferStatus.Accepted:
                t.status = DecentralizedTransferStatus.AcceptedEndorsed
                self.events.append(("TransferResponded", t.payload['id']))
            elif status == DecentralizedTransferStatus.Rejected:
                t.status = DecentralizedTransferStatus.RejectedEndorsed
                self.events.append(("TransferResponded", t.payload['id']))
        #print('post endorsement status:')
        #print(t.is_endorsed)
        #print(t.endorsement)
        #print(t.is_declined)
        return True

    async def decline_action(self, 
                             id: str, 
                             status: DecentralizedTransferStatus) -> bool:
        if id not in self.local_transfers:
            raise KeyError("Transfer not exist")
        t: DecentralizedTransfer = self.local_transfers[id]

        if t.is_declined:
            return True # declined already
        if t.is_endorsed:
            return False # endorsed already

        t.is_declined = True
        return True


class EthereumILStateManager(Web3Initializer, ILStateManager):
    """State manager for interacting with DSM layer of Ethereum
    """
    def __init__(self, cfg: DIBEthereumConfig):
        Web3Initializer.__init__(self, cfg.url, cfg.port, cfg.poa, cfg.ipc_path)

        if cfg.source_timeout:
            self.source_timeout = int(cfg.source_timeout)
        else:
            self.source_timeout = 10
        if cfg.destination_timeout:
            self.destination_timeout = int(cfg.destination_timeout)
        else:
            self.destination_timeout = 10

        self.contract = self.web3.eth.contract(
            abi=cfg.contract_abi, address=cfg.contract_address)
        self.minter = cfg.minter
        self.last_block = self.web3.eth.blockNumber

        self.private_key = cfg.private_key
        self.password = cfg.password

        # transfers that need to be validated by DIL
        self.transfers_to_validate: Dict[str, DecentralizedTransfer] = {}
        
        # transfers to be sent to responder
        self.transfers_to_send: Dict[str, DecentralizedTransfer] = {}
        
        # transfers to be finalized (committed/abordted on initiator side)
        self.transfers_to_finalize: Dict[str, DecentralizedTransfer] = {}
        
        # transfers that has been accepted/rejected by the initiator
        self.transfers_responded: Dict[str, DecentralizedTransfer] = {}

        # transfers in Sent and Confirming state for checking timeouts
        self.transfers_sent: Dict[str, DecentralizedTransfer] = {}
        self.transfers_confirming: Dict[str, DecentralizedTransfer] = {}

        # filters for DSM events
        self.entry_created_filter = self.contract.events.EntryCreated.createFilter(fromBlock='latest')
        self.entry_updated_filter = self.contract.events.EntryUpdated.createFilter(fromBlock='latest')
        
        # keep status of transactions for timeout handling purposes
        self.transactions: Dict[str, DecentralizedTransfer] = {}
        
        # Alternative way of handling events: run a separate thread to check for new DSM events
        #worker = Thread(target=self.log_loop, 
        #                args=(self.entry_created_filter, self.entry_updated_filter, 1), daemon=True)
        #worker.start()


    async def wait_for_events(self):
        event_type = 0
        for event in self.entry_created_filter.get_new_entries():
            self.handle_entry_created(event)
            event_type = event_type | DecentralizedEventType.EntryCreated
                
        for event in self.entry_updated_filter.get_new_entries():
            self.handle_entry_updated(event)
            event_type = event_type | DecentralizedEventType.EntryUpdated

        return event_type
        
        

    def handle_entry_created(self, event):
        """
        Handles EntryCreated events from DSM
        Stores relevant data to transfer object and adds it to transfers_to_validate
        """
        #print("SM: EC event is: ", event)
        #print(event['args'],event['args']['data'])
        #event['event'] == "EntryCreated"
        
        # convert event to transfer
        transfer = DecentralizedTransfer(DecentralizedTransferStatus.Initialized)
        transfer_id = str(event['args']['id']) # id will be string inside interledger
        raw_transfer = self.contract.functions.transfers(int(transfer_id)).call()
        transfer.payload['id'] = transfer_id
        transfer.payload['data'] = raw_transfer[1]
        transfer.metadata['blockNumber'] = raw_transfer[6][0]
        transfer.metadata['transactionHash'] = raw_transfer[6][1].hex()
        transfer.metadata['additionalData'] = raw_transfer[6][2]
        
        self.transfers_to_validate[transfer_id] = transfer


    def handle_entry_updated(self, event):
        """
        Handles EntryUpdated events from DSM
        Stores relevant data to transfer object and adds it to transfers_to_send, 
        transfers_to_validate, or transfers_to_finalize depending on the state of the transfer
        """
        #print("SM: EU event is: ", event)

        # convert event to transfer
        transfer_id = str(event['args']['id']) # id will be string inside interledger
        raw_transfer = self.contract.functions.transfers(int(transfer_id)).call()
        transfer_status = raw_transfer[2]

        now = datetime.now()
        print(f"{now}: SM: received event for id {transfer_id}, state: {transfer_status}")
        
        transfer = DecentralizedTransfer(raw_transfer[2])
        transfer.payload['id'] = transfer_id
        transfer.payload['data'] = raw_transfer[1]
        
        # transfer has been endorsed => it can be sent to Responder
        if transfer_status == DecentralizedTransferStatus.InitializedEndorsed:
            #print("\tEU: InitializedEndorsed event detected")
            self.transfers_to_send[transfer_id] = transfer

        if transfer_status == DecentralizedTransferStatus.Sent:
            transfer.timestamp = now
            self.transfers_sent[transfer_id] = transfer

        # transfer has been Accepted/Rejected by Responder => needs to be validated
        # by other DSM nodes
        if (transfer_status == DecentralizedTransferStatus.Accepted) or \
            (transfer_status == DecentralizedTransferStatus.Rejected): 
                
            transfer.metadata['blockNumber'] = raw_transfer[7][0]
            transfer.metadata['transactionHash'] = raw_transfer[7][1].hex()
            transfer.metadata['additionalData'] = raw_transfer[7][2]
            transfer.metadata['nonce'] = raw_transfer[5]
            
            #print("\tEU: Accepted/Rejected metadata", transfer.metadata)
            self.transfers_to_validate[transfer_id] = transfer
            
            # remove transfer from transfers_sent since it's already moved to a next stage
            self.transfers_sent.pop(transfer_id, None)
        
        # Acception/Rejection has been endorsed => transfer can be finalized on Initiator side
        if (transfer_status == DecentralizedTransferStatus.AcceptedEndorsed) or \
            (transfer_status == DecentralizedTransferStatus.RejectedEndorsed):
            #print("\tEU: AcceptedEndorsed/RejectedEndorsed event detected")
            self.transfers_to_finalize[transfer_id] = transfer

        if transfer_status == DecentralizedTransferStatus.Confirming:
            transfer.timestamp = now
            self.transfers_confirming[transfer_id] = transfer
            
            
        # transfer has been Committed/Aborted by Initiator => needs to be validated 
        # by other DSM nodes
        if (transfer_status == DecentralizedTransferStatus.Committed) or \
            (transfer_status == DecentralizedTransferStatus.Aborted):
            transfer.metadata['blockNumber'] = raw_transfer[8][0]
            transfer.metadata['transactionHash'] = raw_transfer[8][1].hex()
            transfer.metadata['additionalData'] = raw_transfer[8][2]
            #print("\tEU: Committed/Rejected metadata", transfer.metadata)
            self.transfers_to_validate[transfer_id] = transfer
            
            # remove transfer from transfers_confirming since it's already moved to a next stage
            self.transfers_confirming.pop(transfer_id, None)
            
        if (transfer_status == DecentralizedTransferStatus.CommittedEndorsed) or \
            (transfer_status == DecentralizedTransferStatus.AbortedEndorsed):
                # remove transaction from self.transactions since it has been finalized
                self.transactions.pop(transfer_id, None)


    # Alternative way of handling events: run a separate thread to check for new DSM events        
    def log_loop(self, created_filter, updated_filter, poll_interval):
        while True:
            for event in created_filter.get_new_entries():
                self.handle_entry_created(event)
                
            for event in updated_filter.get_new_entries():
                self.handle_entry_updated(event)

            time.sleep(poll_interval)

        
    async def create_entry(self, 
                           id: str, 
                           transfer: DecentralizedTransfer) -> bool:
        
        # TODO: do we need to check things here?
        #t = self.contract.functions.transfers(int(id)).call()
        #if t[0] == id and t[2] != DecentralizedTransferStatus.NotApplicable: # check id and state
        #    return False # exist
        
        try:
            tx_hash = self.contract.functions \
                .createEntry(int(id), Web3.toBytes(transfer.payload['data']), \
                            transfer.metadata['blockNumber'], transfer.metadata['transactionHash'], \
                            transfer.metadata['additionalData']) \
                .transact({'from': self.minter}) 
            #self.web3.eth.waitForTransactionReceipt(tx_hash)
            receipt = await asyncio.get_event_loop().run_in_executor(
                None, functools.partial(self.web3.eth.waitForTransactionReceipt, tx_hash))
        except Exception as e:
            print("state_manager.py: create_entry error", e)
            return False

        return True


    async def signal_send_acceptance(self, 
                                     id: str) -> bool:
        t = self.contract.functions.transfers(int(id)).call()
        if t[0] == 0 or t[2] == DecentralizedTransferStatus.NotApplicable: # check id and state
            raise KeyError("Transfer not exist")

        if (t[2] != DecentralizedTransferStatus.InitializedEndorsed) and \
            (t[2] != DecentralizedTransferStatus.Sent):
                return False # reject, taken by other or in other state

        try:
            tx_hash = self.contract.functions \
                .willingToSendTransfer(int(id)).transact({'from': self.minter})
        except Exception as e: # transaction failed or reverted
            print("state_manager.py: signal_send_acceptance error", e)
            return False
        
        # if transaction is successful, it will return a EntryUpdated(id, status) event.
        # Since we can't check return value from the transaction, we need to check for this event
        #receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
        receipt = await asyncio.get_event_loop().run_in_executor(
            None, functools.partial(self.web3.eth.waitForTransactionReceipt, tx_hash))
        if receipt['status']:
            if receipt['logs']:
                log_id = int(receipt['logs'][0]['topics'][1].hex(), 0)
                log_status = int(receipt['logs'][0]['topics'][2].hex(), 0)
                #print("\tstate_manager.py: logs are: ", log_id, log_status)
                if (log_id == int(id)) and (log_status == DecentralizedTransferStatus.Sent):
                    return True
        return False

    async def signal_confirm_acceptance(self, 
                                        id: str) -> bool:
        t = self.contract.functions.transfers(int(id)).call()
        if t[0] == 0 or t[2] == DecentralizedTransferStatus.NotApplicable: # check id and state
            raise KeyError("Transfer not exist")

        if t[2] != DecentralizedTransferStatus.AcceptedEndorsed and \
            t[2] != DecentralizedTransferStatus.Confirming and \
                t[2] != DecentralizedTransferStatus.RejectedEndorsed:
            return False # reject, due to taken by other

        try:
            tx_hash = self.contract.functions \
                .willingToFinalizeTransfer(int(id)).transact({'from': self.minter})
        except Exception as e: # transaction failed or reverted
            print("state_manager.py: signal_confirm_acceptance error", e)
            return False
        
        # if transaction is successful, it will return a EntryUpdated(id, status) event.
        # Since we can't check return value from the transaction, we need to check for this event
        #receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
        receipt = await asyncio.get_event_loop().run_in_executor(
                None, functools.partial(self.web3.eth.waitForTransactionReceipt, tx_hash))
        if receipt['status']:
            if receipt['logs']:
                log_id = int(receipt['logs'][0]['topics'][1].hex(), 0)
                log_status = int(receipt['logs'][0]['topics'][2].hex(), 0)
                if (log_id == int(id)) and (log_status == DecentralizedTransferStatus.Confirming):
                    return True        
        return False

    async def update_entry(self, 
                           id: str, 
                           status: DecentralizedTransferStatus, 
                           transfer: DecentralizedTransfer = None) -> bool:
        t = self.contract.functions.transfers(int(id)).call()
        if t[0] == 0 or t[2] == DecentralizedTransferStatus.NotApplicable: # check id and state
            raise KeyError("Transfer not exist")
        
        blockNumber = 0
        transactionHash = b''
        additionalData = 0
        nonce = 0

        if (status == DecentralizedTransferStatus.Accepted) or \
            (status == DecentralizedTransferStatus.Rejected):
            blockNumber = transfer.metadata['blockNumber']
            transactionHash = transfer.metadata['transactionHash']
            additionalData = transfer.metadata['additionalData']
            nonce = int(transfer.metadata['nonce'])

        if (status == DecentralizedTransferStatus.Committed) or \
            (status == DecentralizedTransferStatus.Aborted):
            blockNumber = transfer.metadata['blockNumber']
            transactionHash = transfer.metadata['transactionHash']
            
        if status == DecentralizedTransferStatus.Aborted:
            additionalData = ErrorCode.TRANSACTION_FAILURE # reason for aborting

        try:
            tx_hash = self.contract.functions \
                .updateEntry(int(id), status.value, nonce, blockNumber, transactionHash, additionalData) \
                .transact({'from': self.minter})
            #self.web3.eth.waitForTransactionReceipt(tx_hash)
            receipt = await asyncio.get_event_loop().run_in_executor(
                None, functools.partial(self.web3.eth.waitForTransactionReceipt, tx_hash))
        except Exception as e:
            print("state_manager.py: update_entry error", e)
            return False

        return True

    async def endorse_action(self, 
                             id: str, 
                             status: DecentralizedTransferStatus) -> bool:
        t = self.contract.functions.transfers(int(id)).call()
        if t[0] == 0 or t[2] == DecentralizedTransferStatus.NotApplicable: # check id and state
            raise KeyError("Transfer not exist")

        try:
            tx_hash = self.contract.functions \
                .endorseAction(int(id), status) \
                .transact({'from': self.minter})
            #self.web3.eth.waitForTransactionReceipt(tx_hash)
            receipt = await asyncio.get_event_loop().run_in_executor(
                None, functools.partial(self.web3.eth.waitForTransactionReceipt, tx_hash))
            
            # store status of the transaction for timeout handling
            self.transactions[id] = status 
        except Exception as e:
            print("state_manager.py: endorse_action error", e, status, t[2], t[0])
            return False

        #if status in (DecentralizedTransferStatus.Accepted, \
        #    DecentralizedTransferStatus.Rejected): 
        #    t = self.contract.functions.transfers(int(id)).call()
        #    print(f'--> Checking transfer after endorsed... {t}')
        return True

    async def decline_action(self, 
                             id: str, 
                             status: DecentralizedTransferStatus) -> bool:
        t = self.contract.functions.transfers(int(id)).call()
        if t[0] == 0 or t[2] == 0: # check id and state
            raise KeyError("Transfer not exist")

        try:
            tx_hash = self.contract.functions.declineAction(int(id), status) \
                .transact({'from': self.minter})
            #self.web3.eth.waitForTransactionReceipt(tx_hash)
            receipt = await asyncio.get_event_loop().run_in_executor(
                None, functools.partial(self.web3.eth.waitForTransactionReceipt, tx_hash))
        except Exception as e:
            print("state_manager.py: decline_action error", e)
            return False

        return True

