const fs = require("fs");
const ini = require("ini");

const Migrations = artifacts.require("Migrations");
const GameToken = artifacts.require("GameToken");

/**
 * @dev Migration script
 * For networks left, right, compose_left, compose_right:
 *  - deploy the token contract
 *  - update the configuration file ../../local-config.cfg
 * For network development and others: deploy the token contract
 */
module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    alice = accounts[0];
    const presetNetworks = ["left", "right", "compose_left", "compose_right"];

    if (presetNetworks.includes(network)) {
      await deployer.deploy(Migrations);
      const token = await deployer.deploy(GameToken, "GameToken", "GAME", [], {'from': alice});

      const base_path = "ledgers/solidity/contracts/";
      const config = ini.parse(fs.readFileSync('../../configs/dib.cfg', 'utf-8'));
      net_config = config[network]
      net_config.minter = alice
      net_config.contract = token.address
      net_config.contract_abi = base_path + "GameToken.abi.json";

      const iniText = ini.stringify(config);
      fs.writeFileSync('../../configs/dib.cfg', iniText);
      return;
    }

    // Local truffle test network if development is not present in config
    await deployer.deploy(Migrations);
    token = await deployer.deploy(GameToken, "GameToken", "GAME", [], {'from': alice});

  });
};
