const Migrations = artifacts.require("Migrations");
const GameToken = artifacts.require("GameToken");

const fs = require("fs");
const ini = require("ini");

/**
 * Migration script
 * 
 * Network development:
 *  - deploy the token contract
 * 
 * Networks left, right, compose_left, compose_right
 *  - deploy the token contract
 *  - update the configuration file ../../configs/local-config.cfg
 * 
 * Otherwise:
 *  - deploy the token contract
 */
module.exports = function(deployer, network, accounts) {

  deployer.then(async () => {

    alice = accounts[0];
    config_file_path = "../../configs/local-config.cfg";

    if(network == "development") {

      await deployer.deploy(Migrations);
      const token = await deployer.deploy(GameToken, "GameToken", "GAME", [], {'from': alice});
    }
    
    // Since ganache needs to be restarted every time, the deployed contract address will 
    // be different. Store in the local-config file the address of the contract and its minter
    // for testing or demo.
    if(network == "left" || network == "right" ||
        network == "left_compose" || network == "right_compose") {

      await deployer.deploy(Migrations);
      const token = await deployer.deploy(GameToken, "GameToken", "GAME", [], {'from': alice});

      base_path = "ledgers/solidity/contracts/";
      const config = ini.parse(fs.readFileSync(config_file_path, 'utf-8'));
      net_config = config[network]
      net_config.minter = alice
      net_config.contract = token.address
      net_config.contract_abi = base_path + "GameToken.abi.json";

      const iniText = ini.stringify(config);
      fs.writeFileSync(config_file_path, iniText);
    } else {

      // Local truffle test network if development is not present in config
      await deployer.deploy(Migrations);
      token = await deployer.deploy(GameToken, "GameToken", "GAME", [], {'from': alice});

    }
  });
};
